import { enableProdMode, TRANSLATIONS, TRANSLATIONS_FORMAT  } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Constants } from './app/constants';

if (environment.production) {
  enableProdMode();
}

if (localStorage.getItem(Constants.LANGUAGE) == null){
  localStorage.setItem(Constants.LANGUAGE, 'et_EE');
}

const locale = localStorage.getItem(Constants.LANGUAGE);

declare const require;
const translations = require('raw-loader!./locale/messages.'+locale+'.xlf').default;

platformBrowserDynamic().bootstrapModule(AppModule, {
  providers: [
    {provide: TRANSLATIONS, useValue: translations},
    {provide: TRANSLATIONS_FORMAT, useValue: 'xlf'}
  ]
}).catch(err => console.error(err));

