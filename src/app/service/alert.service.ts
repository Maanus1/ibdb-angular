import {Injectable, TemplateRef} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  alerts: any[] = [];
  
  showSuccessAlert(text: string){
  	this.show(text, { classname: 'bg-success text-light toast', delay: 5000 });
  }
  
  showErrorAlert(error: any){
  	if (!error || !error.error ){
  	  return;
  	}
  	let text;
  	
  	if (error.error.error_description){
      text = error.error.error_description;
    }
  	else if(error.error.errors){
  	  text = error.error.errors;
  	}
    if (text != '0' && text != '' && text != null && text != undefined){
      this.show(text, { classname: 'bg-danger text-light toast', delay: 5000 });
    }
  }
  
  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.alerts = []
    this.alerts.push({textOrTpl, ...options});
  }
  
  remove(alert) {
    this.alerts = this.alerts.filter(t => t !== alert);
  }
}
