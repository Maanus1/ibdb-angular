import {Injectable} from '@angular/core';
import {AbstractControl, FormGroup, AsyncValidatorFn, ValidationErrors} from "@angular/forms";
import {map, catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';

import {UserService} from './model-service/user.service'
import {BookService} from './model-service/book.service'
import {User} from '../model/user.model'

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  constructor(private userService: UserService, private bookService: BookService) {
  }

  emailTaken(userId: number): AsyncValidatorFn {
    return (ctrl: AbstractControl): Observable<ValidationErrors> => {
        return this.userService.isEmailTaken({email: ctrl.value, userId: userId}).pipe(
        map(isEmailTaken => (isEmailTaken ? { emailTaken: true } : null)),
        catchError(() => null)
    )};
}

  aliasTaken(userId: number): AsyncValidatorFn { 
    return (ctrl: AbstractControl): Observable<ValidationErrors> => {
        return this.userService.isAliasTaken({alias: ctrl.value,userId: userId}).pipe(
        map(isAliasTaken => (isAliasTaken ? { aliasTaken: true } : null)),
        catchError(() => null)
    )};
}

  isbnTaken = (ctrl: AbstractControl): Observable<ValidationErrors> => {
      return this.bookService.isIsbnTaken(ctrl.value).pipe(
      map(isIsbnTaken => (isIsbnTaken ? { isbnTaken: true } : null)),
      catchError(() => null)
    );
  }
  
  passwordConfirming(c: FormGroup): { invalid: boolean } {
    if (c.get('password').value !== c.get('confirmPassword').value) {
      return {invalid: true};
    }
  }
}
