import {Injectable} from '@angular/core';
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {BookCommentI} from "../../model/book-comment.model";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookCommentService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getBookComments(isbn: string): Observable<BookCommentI[]> {
    return <Observable<BookCommentI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/book-comments', {}, null);
  }
  
  updateBookCommentAsUser(bookComment: BookCommentI): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'book-comments/' + bookComment.commentId + '/by-user', bookComment, null);
  }
  
  updateBookCommentAsAdmin(bookComment: BookCommentI): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'book-comments/' + bookComment.commentId + '/by-admin', bookComment, null);
  }
    
  addBookComment(bookComment: BookCommentI): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.POST, 'book-comments', bookComment, null);
  }
}
