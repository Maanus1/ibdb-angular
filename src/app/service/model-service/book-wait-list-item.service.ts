import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {BookWaitListItemI} from "../../model/book-wait-list-item.model";
import {BookI} from "../../model/book.model";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class BookWaitListItemService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  addToWaitList(waitListItem: BookWaitListItemI): Observable<BookWaitListItemI> {
    return <Observable<BookWaitListItemI>>this.httpRequestsService.httpRequest(RequestType.POST, 'book-wait-list',
      waitListItem, null);
  }

  deleteFromWaitList(waitListId: number): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'book-wait-list/' + 
    waitListId, {}, null);
  }

  getWaitListItem(isbn: string, params: { libraryId?: number } ): Observable<BookWaitListItemI> {
    return <Observable<BookWaitListItemI>>this.httpRequestsService.httpRequest(RequestType.GET, 
    'books/' + isbn + '/book-wait-list/user-item', {}, params);
  }

  getWaitList(params: { pageNumber: number, 
                        pageSize: number,
                        searchPhrase?: string,
                        libraryId?: number } ): Observable<BookWaitListItemI[]> {
    return <Observable<BookWaitListItemI[]>>this.httpRequestsService.httpRequest(RequestType.GET,
      'book-wait-list', {}, params);
  }

  getWaitListCount(params: { searchPhrase?: string,
                             libraryId?: number } ): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET,
      'book-wait-list/count',
      {}, params);
  }

  getUserWaitList(params: { pageNumber: number, 
                            pageSize: number,
                            searchPhrase?: string,
                            libraryId?: number } ): Observable<BookWaitListItemI[]> {
    return <Observable<BookWaitListItemI[]>>this.httpRequestsService.httpRequest(RequestType.GET,
      'book-wait-list/current-user', {}, params);
  }

  getUserWaitListUniqueBooks(params: { pageNumber: number, 
                                        pageSize: number,
                            			searchPhrase?: string,
                            			libraryId?: number } ): Observable<BookI> {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.GET, 
      'book-wait-list/current-user-unique-books', {}, params);
  }

  getUserWaitListCount(params: { searchPhrase?: string,
                            	 libraryId?: number } ): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 
      'book-wait-list/current-user-count', {}, params);
  }
}

