import {Injectable} from '@angular/core';
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {BookCommentI} from "../../model/book-comment.model";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentRatingService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  updateCommentRating(commentRating): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'comment-rating/' + commentRating.commentId, 
    commentRating, null);
  }
  
  addCommentRating(commentRating): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.POST, 'comment-rating', commentRating, null);
  }
    
  deleteCommentRating(id): Observable<BookCommentI> {
    return <Observable<BookCommentI>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'comment-rating/'+id, {}, null);
  }
}
