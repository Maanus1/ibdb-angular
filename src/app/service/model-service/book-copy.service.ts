import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {BookCopyI} from "../../model/book-copy.model";
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class BookCopyService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getBookCopyCount(isbn: string, params: {available?: boolean, libraryId?: number}): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/book-copies/count', {}, params);
  }

  getBookCopiesByUser(isbn: string, params: {libraryId?: number}): Observable<BookCopyI[]> {
    return <Observable<BookCopyI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/book-copies', {}, params);
  }

  getLibraryBookCopies(libraryId: number, params: {pageNumber: number, pageSize: number}): Observable<BookCopyI[]> {
    return <Observable<BookCopyI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'libraries/' + libraryId + '/book-copies', {}, params);
  }

  getLibraryBookCopyCount(libraryId: number): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET,'libraries/' + libraryId + '/book-copies/count', {}, null);
  }

  getBookCopiesByAdmin(isbn: string, 
                       params: { pageNumber: number, 
                                 pageSize: number, 
                                 available?: boolean, 
                                 libraryId?: number }): Observable<BookCopyI[]> {
    return <Observable<BookCopyI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/book-copies/by-admin', {}, params);
  }

  addBookCopy(bookCopy: BookCopyI): Observable<BookCopyI> {
    return <Observable<BookCopyI>>this.httpRequestsService.httpRequest(RequestType.POST, 'book-copies', bookCopy, null);
  }

  updateBookCopy(bookCopy: BookCopyI): Observable<BookCopyI> {
    return <Observable<BookCopyI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'book-copies/' + bookCopy.bookId, bookCopy, null);
  }

  deleteBookCopy(bookId: number): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'book-copies/' + bookId, {}, null);
  }
}

