import {Injectable} from '@angular/core';
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {Observable} from 'rxjs';
import {NotificationI} from "../../model/notification.model";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  markNotificationsAsSeen(): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.PUT, 'notifications/mark-as-seen', {}, null);
  }

  getNotifications(params: {  pageNumber: number, pageSize: number }): Observable<NotificationI[]> {
    return <Observable<NotificationI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'notifications', {}, params);
  }

  getNotificationCount(): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'notifications/count', {}, null);
  }

  getUnreadNotificationCount(): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'notifications/unread-count', {}, null);
  }
}
