import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {Injectable} from "@angular/core";
import {RatingI} from "../../model/rating.model";
import {BookI} from "../../model/book.model";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class RatingService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getRatings(params: { pageNumber: number,
				       pageSize: number,
				       searchPhrase?: string,
				       startAge: number,
				       endAge: number,
				       minimumRating?: number,
				       libraryId?: number }): Observable<RatingI[]> {
    return <Observable<RatingI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'ratings',
      {}, params);
  }

  getRating(isbn: string, params: {startAge: number, endAge: number}): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/rating', {}, params);
  }

  getUserRatingsCount(params: { searchPhrase?: string }): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'ratings/user-ratings-count', {}, params);
  }

  getCurrentUserRatings(params: { pageNumber: number,
							      pageSize: number,
							      searchPhrase?: string }): Observable<RatingI[]> {
    return <Observable<RatingI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'ratings/user-ratings', {}, params);
  }

  getTopRatings(params: { pageSize: number,
					      startAge: number,
					      endAge: number,
					      libraryId?: number }): Observable<BookI[]> {
    return <Observable<BookI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'ratings/top', {}, params);
  }

  getBookWithRating(isbn: string): Observable<BookI> {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/with-rating', {}, null);
  }

  getCurrentUserRating(isbn: string): Observable<RatingI> {
    return <Observable<RatingI>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/user-rating',
     {}, null);
  }

  addRating(rating: RatingI): Observable<RatingI> {
    return <Observable<RatingI>>this.httpRequestsService.httpRequest(RequestType.POST, 'ratings',
      JSON.stringify(rating), null);
  }
  
  updateRating(rating: RatingI): Observable<RatingI> {
    return <Observable<RatingI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'ratings/' + rating.ratingId,
      JSON.stringify(rating), null);
  }
  
  deleteRating(ratingId: number): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'ratings/' + ratingId, {}, null);
  }
}
