import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {LentBookI} from "../../model/lent-book.model";
import {BookI} from "../../model/book.model";
import {UserI} from "../../model/user.model";
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class LentBookService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  addLentBook(lentBook: LentBookI): Observable<LentBookI> {
    return <Observable<LentBookI>>this.httpRequestsService.httpRequest(RequestType.POST, 'lent-books', lentBook, null);
  }

  updateLentBookStatus(lentBookId: number): Observable<LentBookI> {
    return <Observable<LentBookI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'lent-books/' + lentBookId + '/update-lending-status', {}, null);
  }

  cancelBookLending(lentBookId: number): Observable<LentBookI> {
    return <Observable<LentBookI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'lent-books/' + lentBookId + '/cancel-lending-by-user', {}, null);
  }

  cancelBookLendingByAdmin(lentBookId: number): Observable<LentBookI> {
    return <Observable<LentBookI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'lent-books/' + lentBookId + '/cancel-lending-by-admin', {}, null);
  }

  getLendingStatuses(): Observable<string[]> {
    return <Observable<string[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/lending-statuses', {}, null);
  }

  getAllLendingStatuses(): Observable<string[]> {
    return <Observable<string[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/lending-statuses', {}, null);
  }


  getBookReservedDays(bookId: number): Observable<Date[]> {
    return <Observable<Date[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'book-copies/' + bookId + '/lent-books/reserved-days', {}, null);
  }

  getUserLentBookCount(params:{ searchPhrase?: string,
							    lendingStatus?: string,
							    fromDate?: string,
							    toDate?: string,
							    libraryId?: number }): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/user-count', {}, params);
  }

  getCurrentUserLentBooks(params: { pageNumber: number,
								    pageSize: number,
								    searchPhrase?: string,
								    lendingStatus?: string,
								    fromDate?: string,
							     	toDate?: string,
					   			    libraryId?: number }): Observable<LentBookI[]> {
    return <Observable<LentBookI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/user', {}, params);

  }

  getCurrentUserUniqueLentBooks(params: { pageNumber: number,
									      pageSize: number,
									      searchPhrase?: string,
									      lendingStatus?: string,
									      fromDate?: string,
									      toDate?: string,
									      libraryId?: number }) {
    return<Observable<BookI>> this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/user-unique-books', {}, params);
  }

  getLentBookCount(params: { bookSearchPhrase?: string,
						      userSearchPhrase?: string,
						      lendingStatus?: string,
						      fromDate?: string,
						      toDate?: string,
						      libraryId?: number }) {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/count', {}, params);
  }

  getLentBooks(params: { pageNumber: number,
					     pageSize: number,
					     bookSearchPhrase?: string,
					     userSearchPhrase?: string,
					     lendingStatus?: string,
					     fromDate?: string,
					     toDate?: string,
					     libraryId?: number }) {
    return <Observable<LentBookI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books', {}, params);
  }

  getUniqueLentBookUsers(params: { pageNumber: number,
								     pageSize: number,
								     bookSearchPhrase?: string,
								     userSearchPhrase?: string,
								     lendingStatus?: string,
								     fromDate?: string,
								     toDate?: string,
								     libraryId?: number }) {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/unique-users', {}, params);
  }

  getUniqueLentBookBooks(params: { pageNumber: number,
							       pageSize: number,
							       bookSearchPhrase?: string,
							       userSearchPhrase?: string,
							       lendingStatus?: string,
							       fromDate?: string,
							       toDate?: string,
							       libraryId?: number }) {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.GET, 'lent-books/unique-books', {}, params);
  }
}

