import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {UserI} from "../../model/user.model";
import {RoleI} from "../../model/role.model";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getUsers(params: { pageNumber: number,
				     pageSize: number,
				     searchPhrase?: string }): Observable<UserI[]> {
    return <Observable<UserI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'users', {}, params);
  }

  getUserCount(params: { searchPhrase?: string }): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/count', {}, params);
  }
  
  isEmailTaken(params: { email: string, userId: number }): Observable<boolean> {
    return <Observable<boolean>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/is-email-taken', {}, params);
  }
  
  isAliasTaken(params: { alias: string, userId: number }): Observable<boolean> {
    return <Observable<boolean>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/is-alias-taken', {}, params);
  }
  
  getCurrentUser(): Observable<UserI> {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/current', {}, null);
  }
  
  getRoles(): Observable<RoleI[]> {
  	return <Observable<RoleI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/roles', {}, null)
  }
  
  createUser(user: UserI): Observable<UserI> {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.POST, 'users',
      JSON.stringify(user), null);
  }

  changePassword(params: { currentPassword: string, newPassword: string }): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.POST, 
      'users/change-password', {}, params);
  }

  updateUser(user: UserI): Observable<UserI> {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.PUT, 
      'users/' + user.userId+ '/by-user', JSON.stringify(user), null);
  }

  updateUserByAdmin(user: UserI): Observable<UserI> {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.PUT, 
    'users/' + user.userId + '/by-admin', JSON.stringify(user), null);
  }

  deleteUser(userId: number): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'users/' + userId, {}, null);
  }

  getUser(userId: number): Observable<UserI> {
    return <Observable<UserI>>this.httpRequestsService.httpRequest(RequestType.GET, 'users/' + userId, {}, null);
  }
}
