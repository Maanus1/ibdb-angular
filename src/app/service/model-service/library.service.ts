import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {LibraryI} from "../../model/library.model";
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class LibraryService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getAllLibraries(): Observable<LibraryI[]> {
    return <Observable<LibraryI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'libraries', {}, null);
  }

  getAllLibrariesWithPagination(params: { pageNumber: number, pageSize: number }): Observable<LibraryI[]> {
    return <Observable<LibraryI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 
      'libraries', {}, params);
  }

  getLibrary(libraryId: number): Observable<LibraryI> {
    return <Observable<LibraryI>>this.httpRequestsService.httpRequest(RequestType.GET, 'libraries/' + libraryId, {}, null);
  }

  addLibrary(library: LibraryI): Observable<LibraryI> {
    return <Observable<LibraryI>>this.httpRequestsService.httpRequest(RequestType.POST, 'libraries', library, null);
  }

  getLibraryCount(): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'libraries/count', {}, null);
  }

  deleteLibrary(libraryId: number): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'libraries/' + libraryId, {}, null);
  }

  updateLibrary(library: LibraryI): Observable<LibraryI> {
    return <Observable<LibraryI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'libraries/' + library.libraryId, library, null);
  }
  
  compareLibraries(l1: LibraryI, l2: LibraryI){
    if(l1 == null && l2 == null){
      return true;
    }
    if((l1 == null && l2 != null) || (l2 == null && l1 != null)){
      return false;
    }
    return l1.libraryId === l2.libraryId;
  }
}

