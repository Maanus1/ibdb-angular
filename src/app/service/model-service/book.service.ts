import {Injectable} from "@angular/core";
import {HttpRequestsService} from "../http/http-requests.service";
import {RequestType} from "../../enum/request-type";
import {BookI} from "../../model/book.model";
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class BookService {

  constructor(private httpRequestsService: HttpRequestsService) {
  }

  getAllBooksCount(params: { searchPhrase?: string }): Observable<number> {
    return <Observable<number>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/count', {}, params);
  }

  getBooks(params: { searchPhrase?: string, 
             		 pageNumber: number,
      				 pageSize: number }): Observable<BookI[]> {
    return <Observable<BookI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'books', {}, params);
  }
  
  isIsbnTaken( isbn: string ): Observable<boolean> {
    return <Observable<boolean>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn + '/is-isbn-taken', {}, null);
  }
  
  getRecommendedBooks(params: { pageNumber: number,
							    pageSize: number,
							    startAge: number,
							    endAge: number,
							    libraryId?: number }): Observable<BookI[]> {
    return <Observable<BookI[]>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/recommended', {}, params);
  }

  addBook(book: BookI): Observable<BookI> {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.POST, 'books',
      JSON.stringify(book), null);
  }

  updateBook(book: BookI): Observable<BookI> {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.PUT, 'books/' + book.isbn,
      JSON.stringify(book), null);
  }

  deleteBook(isbn: string): Observable<void> {
    return <Observable<void>>this.httpRequestsService.httpRequest(RequestType.DELETE, 'books/' + isbn, 
    {}, null);
  }

  getBook(isbn: string): Observable<BookI> {
    return <Observable<BookI>>this.httpRequestsService.httpRequest(RequestType.GET, 'books/' + isbn, {}, null);
  }

}

