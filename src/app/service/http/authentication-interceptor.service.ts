import {Injectable} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from "../../constants";
import {
  HttpEvent, HttpHandler, HttpRequest,
  HttpInterceptor, HttpErrorResponse
} from "@angular/common/http";
import {Observable, throwError, BehaviorSubject} from 'rxjs';
import {catchError, filter, switchMap, take} from 'rxjs/operators';
import {AuthService} from "../authorization/auth.service";
import {AlertService} from "../alert.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private modalService: NgbModal, private authService: AuthService, private alertService: AlertService) {
  }

  intercept(
    request: HttpRequest<any>, next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.authService.isAuthenticated() && !this.isRefreshing) {
      request = this.addToken(request, this.authService.getAccessToken());
    }
    return next.handle(request).pipe(
      catchError(error => {   
        if (error instanceof HttpErrorResponse && error.status === 401) {
          if (error.error.error_description.includes(Constants.REFRESH_TOKEN_ERROR_DESCRIPTION)) {
            this.authService.logout();
          }
          return this.handle401Error(request, next);
        } else {
          this.alertService.showErrorAlert(error);
          return throwError(error);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      headers: request.headers.set(
        'Authorization',
        'Bearer ' + token
      )
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.authService.refreshToken().pipe(
        switchMap((token: any) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(token['access_token']);
          return next.handle(this.addToken(request, token['access_token']));
        }));
    } else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(accesToken => {
          return next.handle(this.addToken(request, accesToken));
        }));
    }
  }
}
