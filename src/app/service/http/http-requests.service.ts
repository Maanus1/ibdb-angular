import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {RequestType} from "../../enum/request-type";
import {LoggingService} from '../logging.service';
import {Constants} from '../../constants';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HttpRequestsService {

  constructor(private http: HttpClient, private log: LoggingService){
  }

  httpRequest(requestType: RequestType, url: string, body: Object, paramMap: Object = {}): Observable<any>  {


    let httpParams = new HttpParams();

    if (paramMap != undefined) {
      for (const [key, value] of Object.entries(paramMap)) {
        if (value !== null && value !== undefined && value !== '') {
          httpParams = httpParams.set(key, value);
        }
      }
    }
    
    httpParams = httpParams.set('lang',localStorage.getItem(Constants.LANGUAGE));
    
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    const baseUrl = environment.serverUrl;
    switch (requestType) {
      case RequestType.GET:
        return this.http.get(baseUrl + url, {params: httpParams, headers: httpHeaders});
      case RequestType.POST:
        return this.http.post(baseUrl + url, body, {params: httpParams, headers: httpHeaders});
      case RequestType.PUT:
        return this.http.put(baseUrl + url, body, {params: httpParams, headers: httpHeaders});
      case RequestType.DELETE:
        return this.http.delete(baseUrl + url, {params: httpParams, headers: httpHeaders});
      default:
        this.log.warn('Invalid request type. Only GET, POST, DELETE, PUT are allowed');
        return;
    }

  }
}
