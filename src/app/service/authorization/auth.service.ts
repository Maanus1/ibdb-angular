import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router, ActivatedRoute} from "@angular/router";
import { of, Observable, BehaviorSubject } from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from "../../constants";
import {UserI} from "../../model/user.model";
import * as decode from 'jwt-decode';
import {UserService} from '../model-service/user.service'
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly ACCESS_TOKEN = 'ACCESS_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private readonly CURRENT_USER = 'CURRENT_USER_ID';
  loggedIn: BehaviorSubject<boolean> = new BehaviorSubject(this.isAuthenticated());
  
  authHeaders = {
      'Authorization': 'Basic ' + btoa(environment.clientID+':'+environment.clientSecret),
      'Content-type': 'application/x-www-form-urlencoded'
    };
  
  constructor(public jwtHelper: JwtHelperService, 
              public router: Router,
              private activatedRoute: ActivatedRoute, 
              private http: HttpClient, 
              private modalService: NgbModal, 
              private userService: UserService) {
  }

  public isAuthenticated(): boolean {
    return !!this.getAccessToken();
  }
  
   login(username: string, password: string) {
    const body = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password');
    const headers = this.authHeaders;   
    return this.http.post(environment.serverUrl + 'oauth/token', body.toString(), {headers}).pipe(tap(tokens => this.storeTokens(tokens)));
  }
  
  logout() {
  	this.router.navigate(['/']).then(result => {
  	  if(result || result === null){
        this.removeTokens();
      }
    });
  }
 
  
 
  refreshToken() {
  const headers = this.authHeaders;
  const body = new HttpParams()
      .set('refresh_token', this.getRefreshToken())
      .set('grant_type', 'refresh_token');
    return this.http.post(environment.serverUrl + 'oauth/token', body, {headers}).pipe(tap((tokens) => {
      this.storeAccessToken(tokens['access_token']); 
    }));
  }

  getAccessToken() {
    return localStorage.getItem(this.ACCESS_TOKEN);
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeAccessToken(accessToken: string) {
    localStorage.setItem(this.ACCESS_TOKEN, accessToken);
  }

  private storeTokens(tokens) {
    localStorage.setItem(this.ACCESS_TOKEN, tokens['access_token']);
    localStorage.setItem(this.REFRESH_TOKEN, tokens['refresh_token']);  
    this.setCurrentUser();  
  }
  
  getCurrentUser() {
    return localStorage.getItem(this.CURRENT_USER);
  }
  
  setCurrentUser() {
  	this.userService.getCurrentUser().subscribe(result => {
  	  let currentUser = result as UserI;
  	  localStorage.setItem(this.CURRENT_USER, currentUser.userId.toString());
  	  this.loggedIn.next(this.isAuthenticated());
  	  if (this.router.routerState.snapshot.url === '/create-account'){
  	    this.router.navigate(['/']);
  	  }
  	})
  }
  
  private removeTokens() {
    localStorage.removeItem(this.ACCESS_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
    localStorage.removeItem(this.CURRENT_USER);
    this.loggedIn.next(this.isAuthenticated());
  }
  
  getUserRoles() : string[] {
    const token = this.getAccessToken();
    return token ? decode(token)['authorities'] : [];
  }
  
  hasAnyRole(expectedRoles: string[]): boolean {
    const userRoles: string[] = this.getUserRoles();
    if(userRoles && expectedRoles){
      for (let role of expectedRoles) {
        if (userRoles.includes(role.toUpperCase())) {
          return true;
        }
      }
    }
    return false;
  }
}
