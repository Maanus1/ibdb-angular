import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import {AuthService} from '../authorization/auth.service';
import {LoginComponent} from "../../shared/components/login/login.component";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {
  constructor(public authService: AuthService, public router: Router, private modalService: NgbModal) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRoles = route.data.expectedRole;
    if (!this.authService.isAuthenticated()) {
      this.modalService.open(LoginComponent);
      return false;
    }
    return this.authService.hasAnyRole(expectedRoles,);
  }
}
