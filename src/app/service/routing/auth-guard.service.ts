import {Injectable} from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from '../authorization/auth.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from "../../shared/components/login/login.component";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(public authService: AuthService, public router: Router, private modalService: NgbModal) {
  }

  canActivate(): boolean {
    if (!this.authService.isAuthenticated()) {
      this.modalService.open(LoginComponent);
      return false;
    }
    return true;
  }
}
