import {Injectable} from '@angular/core';
import {NgbDate, NgbTimeStruct, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from '../constants';
import {NgbDatePipe} from '../shared/pipe/ngb-date/ngb-date.pipe';
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  constructor(private ngbDatePipe: NgbDatePipe) {
  }

  static convertDatesToNgbDateStructs(bookedDates: Date[]): NgbDateStruct[] {
    let list: NgbDateStruct[] = [];
    for (let i of bookedDates) {
      let date: Date = new Date(i);
      list.push({day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()})
    }
    return list;
  }

  static getIndexOfNgbDate(ngbDate: NgbDate, array: NgbDate[]): number {
    for (let i = 0; i < array.length; i++) {
      if (array[i].year == ngbDate.year && array[i].month == ngbDate.month && array[i].day == ngbDate.day) {
        return i;
      }
    }
    return -1;
  }

  static getFirstMatchingDateInList(list: NgbDate[], fromDate: NgbDate, toDate: NgbDate): NgbDate {
    for (let i of list.sort(UtilsService.sortDate)) {
      let date: NgbDate = NgbDate.from(i); 
      if (date.after(toDate)) {
        return null;
      }
      if (date.after(fromDate)) {
        return i;
      }  
    }
  }
  
  static sortDate(a: NgbDate, b: NgbDate): number {
    if (a.year < b.year) return -1;
    if (a.year > b.year) return 1;
    if (a.month < b.month) return -1;
    if (a.month > b.month) return 1;
    if (a.day < b.day) return -1;
    if (a.day > b.day) return 1;
    return 0;
  }
  
  static ngbTimeStructToString(time: NgbTimeStruct): string {
    if (!time) {
      return null;
    }
    const separator: string = ':';
    const hourValue: string = time.hour.toString();
    const hour: string = hourValue.length === 2 ? hourValue : '0' + hourValue;
    const minuteValue: string = time.minute.toString();
    const minute: string = minuteValue.length === 2 ? minuteValue : '0' + minuteValue;
    const secondValue: string = time.second.toString();
    const second: string = secondValue.length === 2 ? secondValue : '0' + secondValue;
    return `${hour}` + separator + `${minute}` + separator + `${second}`;
  }

  static stringToNgbTimeStruct(timeString: string): NgbTimeStruct {
    if (!timeString) {
      return null;
    }
    const split: string[] = timeString.split(':');
    return {
      hour: parseInt(split[0], 10),
      minute: parseInt(split[1], 10),
      second: parseInt(split[2], 10)
    };
  }
  
  static convertDateStringToDate(date: string): Date {
    if(!date){
      return null;
    }
    const valueArray: string[] = date.split(Constants.DATE_FORMAT_SEPARATOR);
    return new Date(+valueArray[2], +valueArray[1], +valueArray[0]);
  }
  
  static convertDateToString(date: Date): string {
    if (!date) {
      return null;
    }
    const separator: string = Constants.DATE_FORMAT_SEPARATOR;
    const dateValue: string = date.getDate().toString();
    const day: string = dateValue.length === 2 ? dateValue : '0' + dateValue;
    const monthValue: string = date.getMonth().toString();
    const month: string = monthValue.length === 2 ? monthValue : '0' + monthValue;
    const year: string = date.getFullYear().toString();
    return `${day}` + separator + `${month}` + separator + `${year}`;
  }
}
