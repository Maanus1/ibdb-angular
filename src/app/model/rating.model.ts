import {Book, BookI} from "./book.model";
import {User, UserI} from "./user.model";

export interface RatingI {
  ratingId: number;
  book: Book | BookI;
  user: User | UserI;
  rating: number;
}

export class Rating implements RatingI{
  ratingId: number;
  book: Book | BookI;
  user: User | UserI;
  rating: number;
  constructor(rating: number) {
      this.rating = rating;
  }
}
