import {BookComment, BookCommentI} from "./book-comment.model";
import {User} from "./user.model";

export interface CommentRatingI {
  id: number;
  rating: number;
  bookComment: BookComment;
  user: User | number;
}

export class CommentRating implements CommentRatingI {
  id: number;
  rating: number;
  bookComment: BookComment;
  user: User | number;
  
  constructor(comment: BookComment | BookCommentI, rating: number) {
      this.bookComment = comment;
      this.rating = rating;
  }
}