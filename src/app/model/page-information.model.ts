
export interface PageInformationI {
  pageNumber: number;
  pageSize: number;
}

export class PageInformation implements PageInformationI{
  pageNumber: number;
  pageSize: number;
  
  constructor(pageNumber: number, pageSize: number) {
      this.pageNumber = pageNumber;
      this.pageSize = pageSize;
  }
}