import {Library, LibraryI} from "./library.model";
import {Rating} from "./rating.model";
import {Role} from "./role.model";

export interface UserI{
  userId: number;
  email: string;
  alias: string;
  password: string;
  firstName: string;
  lastName: string;
  birthday: string;
  preferredLibrary: Library | LibraryI;
  ratings: Rating[];
  roles: Role[];
}

export class User implements UserI{
  userId: number;
  email: string;
  alias: string;
  password: string;
  firstName: string;
  lastName: string;
  birthday: string;
  preferredLibrary: Library | LibraryI;
  ratings: Rating[];
  roles: Role[];
  
  equals(user: User | UserI){
    if(this.userId != user.userId || this.email != user.email || this.alias != user.alias ||
      this.firstName != user.firstName || this.lastName != user.lastName || 
      this.birthday != user.birthday || this.preferredLibrary != user.preferredLibrary ||
      this.roles.length != user.roles.length){
        return false;
    }
    this.roles.sort((a, b) => a.roleId < b.roleId ? -1 : a.roleId > b.roleId ? 1 : 0);
    user.roles.sort((a, b) => a.roleId < b.roleId ? -1 : a.roleId > b.roleId ? 1 : 0);
    for (let index = 0; index < this.roles.length; index++) {
      if (this.roles[index].roleId != user.roles[index].roleId){
        return false;
      }
    }
    return true;
  }
}
