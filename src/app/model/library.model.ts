import {Book} from "./book.model";

export interface LibraryI {
  libraryId:number;
  address:string;
  latitude: number;
  longitude: number;
  openingTime: string;
  closingTime: string;
  books: Book[];
}

export class Library implements LibraryI{
  libraryId:number;
  address:string;
  latitude: number;
  longitude: number;
  openingTime: string;
  closingTime: string;
  books: Book[];
  
  equals(library: Library | LibraryI){
    return this.libraryId == library.libraryId && this.address == library.address &&
           this.latitude == library.latitude && this.longitude == library.longitude && 
           this.openingTime == library.openingTime && this.closingTime == library.closingTime;
  }
}
