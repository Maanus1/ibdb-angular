import {Library, LibraryI} from "./library.model";
import {Book, BookI} from "./book.model";

export interface BookCopyI {
  bookId: number;
  available: boolean;
  bought: Date;
  library: Library | LibraryI;
  book: Book | BookI;
}

export class BookCopy implements BookCopyI {
  bookId: number;
  available: boolean;
  bought: Date;
  library: Library | LibraryI;
  book: Book | BookI;
  
  constructor(library: Library | LibraryI, book: Book | BookI, available: boolean) {
      this.library = library;
      this.book = book;
      this.available = available;
  }
}
