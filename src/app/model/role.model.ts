export interface RoleI {
  roleId: number;
  name: string;
  checked: boolean;
}

export class Role implements RoleI{
  roleId: number;
  name: string;
  checked: boolean = false;
}
