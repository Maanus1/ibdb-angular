import {User, UserI} from "./user.model";
import {Book, BookI} from "./book.model";
import {CommentRating, CommentRatingI} from "./comment-rating.model";

export interface BookCommentI {
  commentId: number;
  comment: string;
  edited: boolean;
  deleted: boolean;
  book: Book | BookI;
  user: User | UserI;
  timestamp: Date;
  parentComment: BookComment;
  deleteEvent: boolean;
  editEvent: boolean;
  childComments: BookComment[];
  commentRatings: CommentRating[];
}

export class BookComment implements BookCommentI {
  commentId: number;
  comment: string;
  edited: boolean;
  deleted: boolean;
  book: Book | BookI;
  user: User | UserI;
  timestamp: Date;
  parentComment: BookComment;
  deleteEvent: boolean = false;
  editEvent: boolean = false;
  childComments: BookComment[];
  commentRatings: CommentRating[];
  
  constructor(comment: string, book: Book | BookI, parentComment: BookComment) {
      this.comment = comment;
      this.book = book;
      this.parentComment = parentComment;
  }
}
