import {User, UserI} from "./user.model";
import {Book, BookI} from "./book.model";
import {Library, LibraryI} from "./library.model";

export interface BookWaitListItemI {
  waitListId: number;
  user: User | UserI;
  book: Book | BookI;
  library: Library | LibraryI;
}

export class BookWaitListItem implements BookWaitListItemI{
  waitListId: number;
  user: User | UserI;
  book: Book | BookI;
  library: Library | LibraryI;
  
   constructor(library: Library | LibraryI, book: Book | BookI) {
      this.library = library;
      this.book = book;
  }
}
