import {BookCopy} from "./book-copy.model";
import {User} from "./user.model";

export interface LentBookI {
  id: number;
  fromDate: string;
  toDate: string;
  bookCopy: BookCopy;
  user: User;
  lendingStatus: string;
}

export class LentBook implements LentBookI{
  id: number;
  fromDate: string;
  toDate: string;
  bookCopy: BookCopy;
  user: User;
  lendingStatus: string;
  
  constructor(fromDate: string, toDate: string) {
      this.fromDate = fromDate;
      this.toDate = toDate;
  }
}
