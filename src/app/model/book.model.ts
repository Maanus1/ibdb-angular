import {Library} from "./library.model";

export interface BookI {
  isbn: string;
  bookTitle: string;
  bookAuthor: string;
  publicationYear: string;
  publisher: string;
  averageRating: number;
  available: Boolean;
  imageUrl: string;
}

export class Book implements BookI{
  isbn: string;
  bookTitle: string;
  bookAuthor: string;
  publicationYear: string;
  publisher: string;
  averageRating: number;
  available: Boolean;
  imageUrl: string;
  
  equals(book: Book | BookI) {
    return this.isbn == book.isbn && this.bookTitle == book.bookTitle && this.bookAuthor == book.bookAuthor &&
           this.publicationYear == book.publicationYear && this.publisher == book.publisher;
  }
}
