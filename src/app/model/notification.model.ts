import {User} from "./user.model";

export interface NotificationI {
  notificationId: number;
  message: string;
  user: User;
}

export class Notification implements NotificationI{
  notificationId: number;
  message: string;
  user: User;
}