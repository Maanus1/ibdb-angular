import { trigger, state, style, animate, transition, query, animateChild, group } from '@angular/animations';

export const collapseAnimation = [
    trigger('smoothCollapse', [
      state('initial', style({
        height:'0',
        overflow:'hidden',
        opacity:'0'
      })),
      state('final', style({
        opacity:'1',
        overflow: '{{overflow}}'
      }),{params: {overflow: 'hidden'}}),
      transition('initial=>final', animate('{{initialTime}}ms'),{params: {initialTime: '200'}}),
      transition('final=>initial', animate('{{finalTime}}ms'),{params: {finalTime: '200'}}),
      
    ])
  ];

export const collapseAnimation2 = [
    trigger('smoothEnter', [     
      transition(':enter', [style({
        opacity: '0',
        width: 0,
        paddingLeft: 0,
        overflow:'hidden',
        paddingRight: 0
        }),
        animate('300ms', style({ 
        opacity: '1',
        overflow:'hidden',
        width: '*',
        paddingLeft: '*',
        paddingRight: '*'
        }))
      ]),
      transition(':leave', [
        style({ 
        opacity: '1',
        width: '*',
        overflow:'hidden',
        paddingLeft: '*',
        paddingRight: '*'
        }),
        animate('300ms', style({ 
        opacity: '0',
        width: 0,
        overflow:'hidden',
        paddingLeft: 0,
        paddingRight: 0
        }))
      ])
    ])
  ];

export const backgroundColorAnimation = [
    trigger('notification', [
      state('notifications', style({
        backgroundColor: 'red'
      })),
      state('noNotifications', style({
        backgroundColor: '#343a40'
      })),
      transition('* <=> *', [
        animate('0.5s')
      ]),
    ]),
  ] 
 
export const fadeAnimation =  [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(200, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate(200, style({ opacity: 0 }))
      ])
    ])
  ]
 
  
export const validFormAnimation = [
    trigger('validForm', [
      state('valid', style({
        borderLeftWidth: '10px',
        borderLeftColor: '{{validColor}}'
      }),{params: {validColor: 'green'}}),
      state('notTouched', style({
        borderLeftWidth: '10px',
        borderLeftColor: '{{notTouchedColor}}'
      }),{params: {notTouchedColor: '#ced4da'}}),
      state('invalid', style({
        borderLeftWidth: '10px',
        borderLeftColor: '{{invalidColor}}'  
      }),{params: {invalidColor: '#d9534f'}}),
      transition('valid => invalid', [
        animate('0.2s')
      ]),
      transition('invalid => valid', [
        animate('0.2s')
      ]),
      transition('notTouched => valid', [
        animate('0.2s')
      ]),
      transition('notTouched => invalid', [
        animate('0.2s')
      ]),
    ]),
  ]