import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from "./shared/components/index/index.component";
import {LoginComponent} from "./shared/components/login/login.component";
import { PageNotFoundComponent } from './shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path: '', component: IndexComponent},
  {path: 'page-not-found', component: PageNotFoundComponent},
  {path: 'login', component: LoginComponent},
  /*{path: '**', redirectTo: 'page-not-found',  pathMatch: 'full'},*/
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
