import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http";
import {NgbDateCustomParserFormatter} from "./service/format/ngb-date-custom-parser-formatter";
import {AuthenticationInterceptor} from './service/http/authentication-interceptor.service';
import {Constants} from './constants';
import localeEt from '@angular/common/locales/et';
import { registerLocaleData } from '@angular/common';
import {CanDeactivateGuardService as CanDeactivateGuard} from "./service/routing/can-deactivate-guard.service";
import { EmployeeModule } from './employee/employee.module';
import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import {JwtHelperService} from "@auth0/angular-jwt";
import { NgbDatePipe } from './shared/pipe/ngb-date/ngb-date.pipe';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

export function getCurentLocale(): string {
  return localStorage.getItem(Constants.LANGUAGE);
}
registerLocaleData(localeEt);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    EmployeeModule,
    SharedModule,
    UserModule
  ],
  providers: [
    JwtHelperService, 
    NgbDatePipe,
    {
      provide: NgbDateParserFormatter, 
      useClass: NgbDateCustomParserFormatter
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    { provide: LOCALE_ID, 
      useValue: getCurentLocale() 
    }],
  bootstrap: [AppComponent]
})

export class AppModule {
}

