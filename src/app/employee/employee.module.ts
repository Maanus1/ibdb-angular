import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UpdateBookComponent} from './components/view-books/update-book/update-book.component';
import {UpdateUserComponent} from './components/view-users/update-user/update-user.component';
import {ViewUsersComponent} from './components/view-users/view-users.component';
import {ViewBooksComponent} from "./components/view-books/view-books.component";
import {ViewLendingHistoryComponent} from './components/view-lending-history/view-lending-history.component';
import {UpdateUserConfirmationComponent} from './components/view-users/update-user/update-user-confirmation/update-user-confirmation.component';
import {DeleteBookConfirmationComponent} from './components/view-books/update-book/delete-book-confirmation/delete-book-confirmation.component';
import {UpdateBookConfirmationComponent} from './components/view-books/update-book/update-book-confirmation/update-book-confirmation.component';
import {DeleteUserConfirmationComponent} from './components/view-users/update-user/delete-user-confirmation/delete-user-confirmation.component';
import {DeleteBookCopyConfirmationComponent} from './components/modal/delete-book-copy-confirmation/delete-book-copy-confirmation.component';
import {UpdateReservationStatusConfirmationComponent} from './components/view-lending-history/update-reservation-status-confirmation/update-reservation-status-confirmation.component';
import {UpdateBookCopyConfirmationComponent} from './components/modal/update-book-copy-confirmation/update-book-copy-confirmation.component';
import {ViewLibrariesComponent} from './components/view-libraries/view-libraries.component';
import {UpdateLibraryComponent} from './components/view-libraries/update-library/update-library.component';
import {UpdateLibraryConfirmationComponent} from './components/view-libraries/update-library/update-library-confirmation/update-library-confirmation.component';
import {DeleteLibraryConfirmationComponent} from './components/view-libraries/update-library/delete-library-confirmation/delete-library-confirmation.component';
import {ViewBookWaitListComponent} from './components/view-book-wait-list/view-book-wait-list.component';
import { BookFormComponent } from './components/forms/book-form/book-form.component';
import { LibraryFormComponent } from './components/forms/library-form/library-form.component';
import { BookCopyTableComponent } from './components/tables/book-copy-table/book-copy-table.component';
import { BookTableComponent } from './components/tables/book-table/book-table.component';
import { UserTableComponent } from './components/tables/user-table/user-table.component';
import { WaitListTableComponent } from './components/tables/wait-list-table/wait-list-table.component';
import { LendingHistoryTableComponent } from './components/tables/lending-history-table/lending-history-table.component';
import { LibraryTableComponent } from './components/tables/library-table/library-table.component';
import { SharedModule } from '../shared/shared.module';
import {EmployeeRoutingModule} from "./employee-routing.module";
import { DataUpdateCancellationConfirmationComponent } from './components/modal/data-update-cancellation-confirmation/data-update-cancellation-confirmation.component';
import { HighlightElementDirective } from './directive/highlight-element.directive';


@NgModule({
  declarations: [
    UpdateBookComponent,
    UpdateUserComponent,
    ViewUsersComponent,
    ViewBooksComponent,
    ViewLendingHistoryComponent,
    UpdateUserConfirmationComponent,
    DeleteBookConfirmationComponent,
    UpdateBookConfirmationComponent,
    DeleteUserConfirmationComponent,
    DeleteBookCopyConfirmationComponent,
    UpdateReservationStatusConfirmationComponent,
    UpdateBookCopyConfirmationComponent,
    ViewLibrariesComponent,
    UpdateLibraryComponent,
    UpdateLibraryConfirmationComponent,
    DeleteLibraryConfirmationComponent,
    ViewBookWaitListComponent,
    BookFormComponent,
    LibraryFormComponent,
    BookCopyTableComponent,
    BookTableComponent,
    UserTableComponent,
    WaitListTableComponent,
    LendingHistoryTableComponent,
    LibraryTableComponent,
    DataUpdateCancellationConfirmationComponent, 
    HighlightElementDirective
  ],
  imports: [
    SharedModule,
    EmployeeRoutingModule
  ]
})
export class EmployeeModule { }
