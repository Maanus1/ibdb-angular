import { Directive, HostBinding, HostListener} from '@angular/core';

@Directive({
  selector: '[appHighlightElement]'
})
export class HighlightElementDirective{
  @HostBinding('style.backgroundColor') backgroundColor: string;
  
  
  constructor() {
    }
    
  @HostListener('mouseenter') mouseEnter() {
    this.backgroundColor = 'lightgrey';
  }
  
  @HostListener('mouseleave') mouseLeave() {
      this.backgroundColor = null;
  }
}
