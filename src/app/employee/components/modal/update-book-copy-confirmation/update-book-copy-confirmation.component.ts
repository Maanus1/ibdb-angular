import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookI} from "../../../../model/book.model";
import {BookCopyI} from "../../../../model/book-copy.model";

@Component({
  selector: 'app-update-book-copy-confirmation',
  templateUrl: './update-book-copy-confirmation.component.html'
})
export class UpdateBookCopyConfirmationComponent implements OnInit {
  updatedBookCopy: BookCopyI;
  updatedBook: BookI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  updateBookCopy() {
    this.activeModal.close(true);
  }
}
