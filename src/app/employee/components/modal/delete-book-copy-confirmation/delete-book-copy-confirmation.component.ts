import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookI} from "../../../../model/book.model";

@Component({
  selector: 'app-delete-book-copy-confirmation',
  templateUrl: './delete-book-copy-confirmation.component.html'
})
export class DeleteBookCopyConfirmationComponent implements OnInit {
  bookCopyId: number;
  updatedBook: BookI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  deleteBookCopy() {
    this.activeModal.close(true);
  }
}
