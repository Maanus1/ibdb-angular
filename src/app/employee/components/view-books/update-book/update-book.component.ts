import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BookI} from "../../../../model/book.model";
import {LibraryI} from "../../../../model/library.model";
import {BookCopyI, BookCopy} from "../../../../model/book-copy.model";
import {PageInformation} from "../../../../model/page-information.model";
import {ActivatedRoute, Router} from "@angular/router";
import {BookService} from "../../../../service/model-service/book.service";
import {LibraryService} from "../../../../service/model-service/library.service";
import {BookCopyService} from "../../../../service/model-service/book-copy.service";
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {UpdateBookConfirmationComponent} from './update-book-confirmation/update-book-confirmation.component';
import {DeleteBookConfirmationComponent} from './delete-book-confirmation/delete-book-confirmation.component';
import {DeleteBookCopyConfirmationComponent} from '../../modal/delete-book-copy-confirmation/delete-book-copy-confirmation.component';
import {UpdateBookCopyConfirmationComponent} from '../../modal/update-book-copy-confirmation/update-book-copy-confirmation.component';
import {Constants} from "../../../../constants";
import {AlertService} from "../../../../service/alert.service";
import { collapseAnimation } from '../../../../animations';
import { UserMessages } from '../../../../user-messages';
import {forkJoin, Observable} from 'rxjs';
import {BookFormComponent} from "../../forms/book-form/book-form.component";
import {DataUpdateCancellationConfirmationComponent} from '../../modal/data-update-cancellation-confirmation/data-update-cancellation-confirmation.component';
import {CanComponentDeactivate} from "../../../../service/routing/can-deactivate-guard.service";

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  animations: [collapseAnimation]
})
export class UpdateBookComponent implements OnInit, CanComponentDeactivate {
  @Input() updatedBook: BookI;
  @Input() updatedBookCopy: BookCopyI;
  @Input() bookCopyId: number;
  @ViewChild(BookFormComponent, {static: false}) 
  private bookForm: BookFormComponent;
  id: string;
  book: BookI;
  bookCopyAvailability: boolean = true;
  library: LibraryI;
  libraries: LibraryI[];
  bookCopyCount: number = 0;
  bookCopies: BookCopyI[];
  pageInformation: PageInformation;
  pageSizeOptions: number[];
  bookAvailable: boolean[] = [true, false];
  isCollapsed: boolean = false;
  isCollapsedAddBookCopy: boolean = true;
  isCollapsedFilterBookCopies: boolean = true;
  bookCopyFilterAvailability: boolean = null;
  bookCopyFilterLibrary: LibraryI = null;
  bookAvailabilityMap: Record<string, string>;
  formSubmitted: boolean = false;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private bookService: BookService, private bookCopyService: BookCopyService, private libraryService: LibraryService, private modalService: NgbModal, private alertService: AlertService) {
  }


  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.bookAvailabilityMap = Constants.AVAILABILITY;
    const librariesObservable: Observable<LibraryI[]> = this.libraryService.getAllLibraries();
    const bookObservable: Observable<BookI> = this.bookService.getBook(this.id);
    const observables: Observable<BookI | LibraryI[]>[] = [librariesObservable]
    if (this.id) {
      observables.push(bookObservable);
      this.isCollapsed = true;
      this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
      }
      
    forkJoin(observables)
    .subscribe(([libraries, book]) => {
      this.libraries = libraries as LibraryI[];
      this.library = this.libraries[0];
      this.book = book as BookI;
      if(this.id){
        this.getCurrentBookCount();
      }
    })
  }
  
  getLibraryService() {
      return this.libraryService;
  }
  
  canDeactivate(): boolean | Promise<boolean>{
    if(this.formSubmitted || !this.bookForm.isDirty()){
      return true;
    }
    else{
       const modalRef: NgbModalRef = this.modalService.open(DataUpdateCancellationConfirmationComponent);
       return modalRef.result;
    }
  }
  
  getBookCopyData() {
    this.getCurrentBookCount();
    this.getBookCopies(this.pageInformation);
  }

  resetFilter() {
    this.bookCopyFilterLibrary = null;
    this.bookCopyFilterAvailability = null;
    this.getBookCopyData();
  }

  changeBookCopyAddWindowCollapse() {
    this.isCollapsedAddBookCopy = !this.isCollapsedAddBookCopy;
    this.isCollapsedFilterBookCopies = true;
  }

  changeBookCopyFilterWindowCollapse() {
    this.isCollapsedFilterBookCopies = !this.isCollapsedFilterBookCopies;
    this.isCollapsedAddBookCopy = true;
  }

  addBookCopy() {
    const bookCopy: BookCopy = new BookCopy(this.library, this.book, this.bookCopyAvailability);
    this.bookCopyService.addBookCopy(bookCopy).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.ADD_BOOK_COPY_MESSAGE);
      this.getBookCopyData();
    });

  }
  
  deleteBookCopy(id: number){
    this.bookCopyService.deleteBookCopy(id).subscribe(() => {
          this.alertService.showSuccessAlert(UserMessages.DELETE_BOOK_COPY_MESSAGE);
          this.getBookCopyData();
    }); 
  }
  
  onDeleteBookCopy(bookCopy: BookCopy) {
    const modalRef: NgbModalRef = this.modalService.open(DeleteBookCopyConfirmationComponent);
    modalRef.componentInstance.updatedBook = this.book;
    modalRef.componentInstance.bookCopyId = bookCopy.bookId;
    modalRef.result.then((result) => {
      if (result) {
        this.deleteBookCopy(bookCopy.bookId);
      }
    });
  }
  
  updateBookCopy(bookCopy: BookCopyI){
    this.bookCopyService.updateBookCopy(bookCopy).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_BOOK_COPY_MESSAGE);
      this.getBookCopyData();
    });
  }
  
  onUpdateBookCopy(bookCopy: BookCopyI) {
    const modalRef: NgbModalRef = this.modalService.open(UpdateBookCopyConfirmationComponent);
    modalRef.componentInstance.updatedBook = this.book;
    modalRef.componentInstance.updatedBookCopy = bookCopy;
    modalRef.result.then((result) => {
      if (result) {
        this.updateBookCopy(bookCopy);
      }
    });
  }

  getCurrentBookCount() {
    this.bookCopyService.getBookCopyCount(this.id, {
      available: this.bookCopyFilterAvailability,
      libraryId: (this.bookCopyFilterLibrary ? this.bookCopyFilterLibrary.libraryId : null)
    })
      .subscribe(result => this.bookCopyCount = result as number);
  }

  getBookCopies(pageInformation: PageInformation) {
    this.pageInformation = pageInformation;
    this.bookCopyService.getBookCopiesByAdmin(this.id, {
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize,
      available: this.bookCopyFilterAvailability,
      libraryId: (this.bookCopyFilterLibrary ? this.bookCopyFilterLibrary.libraryId : null)
    }).subscribe(result =>
      this.bookCopies = result as BookCopy[]);
  }
  
  saveBook(book: BookI){
    this.formSubmitted = true;
    if(this.id){
      this.onUpdateBook(book);
    }
    else{
      this.addBook(book);
    }
  }
  
  addBook(book: BookI) {
    this.bookService.addBook(book).subscribe(() => {
      this.router.navigate(['view-books']);
      this.alertService.showSuccessAlert(UserMessages.ADD_BOOK_MESSAGE);
      });
  }
  
  updateBook(book: BookI){
    this.bookService.updateBook(book).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_BOOK_MESSAGE);
      this.router.navigate(['view-books'])
    });
  } 
  
  onUpdateBook(book: BookI) {
    const modalRef = this.modalService.open(UpdateBookConfirmationComponent);
    modalRef.componentInstance.updatedBook = book;
    modalRef.componentInstance.book = this.book;
     modalRef.result.then((result) => {
       if (result) {
         this.updateBook(book);
       }
    });
  }
  
  
  deleteBook(book: BookI){
    this.bookService.deleteBook(book.isbn).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.DELETE_BOOK_MESSAGE);
      this.router.navigate(['view-books']);
    });
  }

  onDeleteBook(book: BookI) {
    const modalRef = this.modalService.open(DeleteBookConfirmationComponent);
    modalRef.componentInstance.updatedBook = book; 
    modalRef.result.then((result) => {
      if (result) {
        this.deleteBook(book);
      }
    });
  }
}
