import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookI} from "../../../../../model/book.model";

@Component({
  selector: 'app-delete-book-confirmation',
  templateUrl: './delete-book-confirmation.component.html'
})
export class DeleteBookConfirmationComponent implements OnInit {
  updatedBook: BookI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }
  
  close() {
    this.activeModal.close(false);
  }
  
  deleteBook() {
    this.activeModal.close(true);
  }
}
