import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BookI} from "../../../../../model/book.model";

@Component({
  selector: 'app-update-book-confirmation',
  templateUrl: './update-book-confirmation.component.html'
})
export class UpdateBookConfirmationComponent implements OnInit {
  book: BookI;
  updatedBook: BookI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }
  
  close() {
    this.activeModal.close(false);
  }

  updateBook() {
    this.activeModal.close(true);  
  }
}
