import {Component, OnInit} from '@angular/core';
import {BookI} from "../../../model/book.model";
import {PageInformationI} from "../../../model/page-information.model";
import {BookService} from "../../../service/model-service/book.service";
import {Constants} from "../../../constants";
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap, map} from 'rxjs/operators';
import {UtilsService} from "../../../service/utils.service";

@Component({
  selector: 'app-view-books',
  templateUrl: './view-books.component.html'
})
export class ViewBooksComponent implements OnInit {
  books: BookI[];
  book: Observable<BookI[]>;
  bookCount: number;
  pageInformation: PageInformationI;
  pageSizeOptions: number[];
  searchPhrase: string = "";

  constructor(private bookService: BookService) {
  }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getPageData(this.searchPhrase);
  }

  filterBooks(event) {
    this.searchPhrase = event.item ? event.item.isbn : event.target.value;
    this.getBooks(this.pageInformation);
    this.getPageData(this.searchPhrase);
  }

  getPageData(searchPhrase: string) {
    this.bookService.getAllBooksCount({searchPhrase: searchPhrase}).subscribe(result => this.bookCount = result as number);
  }

  inputFormatter(book) {
    return book.bookTitle;
  }
  
  resultFormatter(book) {
    return book.bookTitle + ', ' + book.bookAuthor + ',  ' + book.publisher + ', ' + book.publicationYear + ', ISBN: ' + book.isbn
  }
  
  getBooks(pageInformation: PageInformationI) {
    this.pageInformation = pageInformation;
    this.bookService.getBooks({
      searchPhrase: this.searchPhrase,
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize
    })
      .subscribe(result => this.books = result as BookI[]);
  }

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.bookService.getBooks({
        searchPhrase: searchText,
        pageNumber: 1,
        pageSize: 5
      }))
    );
  };
}
