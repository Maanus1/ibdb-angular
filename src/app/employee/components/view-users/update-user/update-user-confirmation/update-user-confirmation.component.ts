import {Component, OnInit} from '@angular/core';
import {UserI} from "../../../../../model/user.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-user-confirmation',
  templateUrl: './update-user-confirmation.component.html'
})
export class UpdateUserConfirmationComponent implements OnInit {
  user: UserI;
  updatedUser: UserI;
  userId: number;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  updateUser() {
    this.activeModal.close(true);
  }

}
