import {Component, OnInit} from '@angular/core';
import {UserI} from "../../../../../model/user.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-user-confirmation',
  templateUrl: './delete-user-confirmation.component.html'
})
export class DeleteUserConfirmationComponent implements OnInit {
  user: UserI;
  userId: number;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  deleteUser() {
    this.activeModal.close(true);
  }
}
