import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {UserI} from "../../../../model/user.model";
import {UserService} from "../../../../service/model-service/user.service";
import {UserFormComponent} from "../../../../shared/components/forms/user-form/user-form.component";
import {LibraryService} from "../../../../service/model-service/library.service";
import {DeleteUserConfirmationComponent} from './delete-user-confirmation/delete-user-confirmation.component';
import {UpdateUserConfirmationComponent} from './update-user-confirmation/update-user-confirmation.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {RoleI} from "../../../../model/role.model";
import {LibraryI} from "../../../../model/library.model";
import {UtilsService} from "../../../../service/utils.service";
import {AlertService} from "../../../../service/alert.service";
import { UserMessages } from '../../../../user-messages';
import {forkJoin, Observable} from 'rxjs';
import {CanComponentDeactivate} from "../../../../service/routing/can-deactivate-guard.service";
import {DataUpdateCancellationConfirmationComponent} from '../../modal/data-update-cancellation-confirmation/data-update-cancellation-confirmation.component';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html'
})
export class UpdateUserComponent implements OnInit, CanComponentDeactivate {
  @Input() userId: number;
  @Input() updatedUser: UserI;
  @ViewChild(UserFormComponent, {static: false}) 
  private userForm: UserFormComponent;
  id: number;
  user: UserI;
  formSubmitted: boolean = false;
  roleList: RoleI[] = [];
  libraries: LibraryI[] = []
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private userService: UserService, private modalService: NgbModal, private alertService: AlertService, private libraryService: LibraryService) {  
  }


  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    const roleObservable: Observable<RoleI[]> = this.userService.getRoles();
    const librariesObservable: Observable<LibraryI[]> = this.libraryService.getAllLibraries();
    const userObservable: Observable<UserI> = this.userService.getUser(this.id);
    const observables: Observable<RoleI[] | LibraryI[] | UserI>[] = [roleObservable, librariesObservable]
    if (this.id){
      observables.push(userObservable);
    }
    forkJoin(observables)
    .subscribe(([roles, libraries, user]) => {
      this.user = user as UserI;
      this.libraries = libraries as LibraryI[];
      this.roleList = roles as RoleI[];
      if(this.id){
        for (let role of this.roleList) {
	      if (this.user.roles.some(x => x.name === role.name)) {
	       role.checked = true;
	      }
	    }
	  }
    });
  }
  
  canDeactivate(): boolean | Promise<boolean>{
    if(this.formSubmitted || !this.userForm.isDirty()){
      return true;
    }
    else{
       const modalRef: NgbModalRef = this.modalService.open(DataUpdateCancellationConfirmationComponent);
        return modalRef.result;
    }
  }
  
  updateUser(user: UserI){
    this.userService.updateUserByAdmin(user).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_USER_MESSAGE);
      this.router.navigate(['view-users'])
    });
  }
  
  onUpdateUser(user: UserI) {
    const modalRef: NgbModalRef = this.modalService.open(UpdateUserConfirmationComponent);
    modalRef.componentInstance.updatedUser = user;
    modalRef.componentInstance.user = this.user;
    modalRef.result.then((result) => {   
      if(result){
        this.updateUser(user);
      }
    });
  }
  
  saveUser(user: UserI){
    this.formSubmitted = true;
    if(user.userId){
      this.onUpdateUser(user);
    }
    else{
      this.createUser(user);
    }
  }
  
  createUser(user: UserI) {
    this.userService.createUser(user)
      .subscribe(() =>{
      this.router.navigate(['view-users']);
      this.alertService.showSuccessAlert(UserMessages.ADD_USER_MESSAGE)});
  }
  
  deleteUser(userId: number){
    this.userService.deleteUser(userId).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.DELETE_USER_MESSAGE);
      this.router.navigate(['view-users']);
    });
  }
  
  onDeleteUser(user: UserI) {
    const modalRef: NgbModalRef = this.modalService.open(DeleteUserConfirmationComponent);
    modalRef.componentInstance.user = user;
    modalRef.result.then((result) => {
      if (result) {
        this.deleteUser(user.userId);
      }
    });
  }
}