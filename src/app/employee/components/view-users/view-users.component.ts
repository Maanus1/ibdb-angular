import {Component, OnInit} from '@angular/core';
import {UserI} from "../../../model/user.model";
import {PageInformationI} from "../../../model/page-information.model";
import {UserService} from "../../../service/model-service/user.service";
import {FormBuilder} from "@angular/forms";
import {Constants} from "../../../constants";
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap, map} from 'rxjs/operators';
import {UtilsService} from "../../../service/utils.service";

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html'
})
export class ViewUsersComponent implements OnInit {
  users: UserI[];
  userCount: number;
  pageInformation: PageInformationI;
  searchPhrase: string = "";
  userModel: Observable<UserI[]>;
  
  constructor(private fb: FormBuilder, private userService: UserService) {
  }

  ngOnInit() {
    this.getPageData(this.searchPhrase);
  }

  filterUsers(event) {
    this.searchPhrase = event.item ? event.item.email : event.target.value;
    this.getPageData(this.searchPhrase);
    this.getUsers(this.pageInformation);
  }

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(), 
      switchMap((searchText) => this.userService.getUsers({
        pageNumber: 1,
        pageSize: 5,
        searchPhrase: searchText
      }))
    );
  };

  resultFormatter(user) {
    return user.email + ', ' + user.alias + ', ' + user.firstName + ' ' + user.lastName + ', ' + user.birthday
  }
  
  inputFormatter(user){
    return user.email;
  }
  
  getPageData(searchPhrase: string) {
    this.userService.getUserCount({searchPhrase: searchPhrase}).subscribe(result =>
      this.userCount = result as number);
  }

  getUsers(pageInformation: PageInformationI) {
    this.pageInformation = pageInformation;
    this.userService.getUsers({
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize,
      searchPhrase: this.searchPhrase
    })
      .subscribe(result => this.users = result as UserI[]);
  }
}
