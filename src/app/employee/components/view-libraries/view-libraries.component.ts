import {Component, OnInit} from '@angular/core';
import {LibraryService} from "../../../service/model-service/library.service";
import {LibraryI} from "../../../model/library.model";
import {Constants} from "../../../constants";
import {UtilsService} from "../../../service/utils.service";
import {PageInformationI} from "../../../model/page-information.model";

@Component({
  selector: 'app-view-libraries',
  templateUrl: './view-libraries.component.html'
})
export class ViewLibrariesComponent implements OnInit {
  libraries: LibraryI[];
  libraryCount: number;
  pageInformation: PageInformationI;
  pageSizeOptions: number[];

  constructor(private libraryService: LibraryService) {
  }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getPageData();
  }
  
  trackByLibrary(index: number, library: LibraryI): number { return library.libraryId; }

  getPageData() {
    this.libraryService.getLibraryCount().subscribe(result =>
      this.libraryCount = result as number);
  }

  getLibraries(pageInformation: PageInformationI) {
    this.pageInformation = pageInformation;
    this.libraryService.getAllLibrariesWithPagination({
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize
    }).subscribe(result =>
      this.libraries = result as LibraryI[]);
  }
}
