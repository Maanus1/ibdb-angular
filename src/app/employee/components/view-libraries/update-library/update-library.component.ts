import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {LibraryService} from "../../../../service/model-service/library.service";
import {BookCopyService} from "../../../../service/model-service/book-copy.service"
import {AuthService} from "../../../../service/authorization/auth.service"
import {LibraryI} from "../../../../model/library.model";
import {BookI} from "../../../../model/book.model";
import {PageInformationI} from "../../../../model/page-information.model";
import {BookCopyI} from "../../../../model/book-copy.model";
import {UpdateLibraryConfirmationComponent} from './update-library-confirmation/update-library-confirmation.component';
import {DeleteLibraryConfirmationComponent} from './delete-library-confirmation/delete-library-confirmation.component';
import {Constants} from "../../../../constants";
import {DeleteBookCopyConfirmationComponent} from '../../modal/delete-book-copy-confirmation/delete-book-copy-confirmation.component';
import {UpdateBookCopyConfirmationComponent} from '../../modal/update-book-copy-confirmation/update-book-copy-confirmation.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {UtilsService} from "../../../../service/utils.service";
import {AlertService} from "../../../../service/alert.service";
import { collapseAnimation, validFormAnimation } from '../../../../animations';
import { UserMessages } from '../../../../user-messages';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons';
import {DataUpdateCancellationConfirmationComponent} from '../../modal/data-update-cancellation-confirmation/data-update-cancellation-confirmation.component';
import {CanComponentDeactivate} from "../../../../service/routing/can-deactivate-guard.service";
import {LibraryFormComponent} from "../../forms/library-form/library-form.component";

@Component({
  selector: 'app-update-library',
  templateUrl: './update-library.component.html',
  animations: [collapseAnimation, validFormAnimation]
})
export class UpdateLibraryComponent implements OnInit, CanComponentDeactivate {
  @Input() updatedLibrary: LibraryI;
  @Input() updatedBook: BookI;
  @Input() updatedBookCopy: BookCopyI;
  @ViewChild(LibraryFormComponent, {static: false}) 
  private libraryForm: LibraryFormComponent;
  id: number;
  pageInformation: PageInformationI;
  library: LibraryI;
  libraries: LibraryI[];
  bookCopies: BookCopyI[];
  isCollapsed: boolean = false;
  bookAvailableMap: Record<string, string>;
  bookCopyCount: number;
  isAdmin: boolean = false;
  faCaretDown = faCaretDown;
  formSubmitted: boolean = false;
  
  constructor(private activatedRoute: ActivatedRoute, private router: Router,
              private libraryService: LibraryService, private bookCopyService: BookCopyService,
              private modalService: NgbModal, private alertService: AlertService, private authService: AuthService) {
  }

  ngOnInit() {
    this.bookAvailableMap = Constants.AVAILABILITY;
    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id != null) {
      this.isCollapsed = true;
      this.getLibraries();
      this.getPageData();
      this.getLibrary();
    }
    this.isAdmin = this.authService.hasAnyRole(['ROLE_ADMIN']);
  }
  
  canDeactivate(): boolean | Promise<boolean>{
    if(this.formSubmitted || !this.libraryForm.isDirty()){
      return true;
    }
    else{
       const modalRef: NgbModalRef = this.modalService.open(DataUpdateCancellationConfirmationComponent);
       return modalRef.result;
    }
  }
  
  getBookCopyData(){
    this.getPageData();
    this.getBookCopies(this.pageInformation);
  }
  
  getLibraryService(){
    return this.libraryService;
  }
  
  getLibrary() {
    this.libraryService.getLibrary(this.id).subscribe(result => {
      this.library = result as LibraryI;
    });
  }

  saveLibrary(library){
    this.formSubmitted = true;
    if(this.id){
      this.onUpdateLibrary(library);
    }
    else{
      this.addLibrary(library);
    }
  }

  addLibrary(library: LibraryI) {
    this.libraryService.addLibrary(library)
      .subscribe(() => {
      this.router.navigate(['view-libraries']);
      this.alertService.showSuccessAlert(UserMessages.ADD_LIBRARY_MESSAGE);});
  }

  getBookCopies(pageInformation: PageInformationI) {
    this.pageInformation = pageInformation;
    this.bookCopyService.getLibraryBookCopies(this.id, {
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize
    })
      .subscribe(result => this.bookCopies = result as BookCopyI[]);
  }
  
  updateLibrary(library: LibraryI){
    this.libraryService.updateLibrary(library).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_LIBRARY_MESSAGE);
      this.router.navigate(['view-libraries']);
    });
  }
  
  onUpdateLibrary(library: LibraryI) {
    const modalRef: NgbModalRef = this.modalService.open(UpdateLibraryConfirmationComponent);
    modalRef.componentInstance.updatedLibrary = library;
    modalRef.componentInstance.library = this.library;
    modalRef.result.then((result) => {
      if (result) {
        this.updateLibrary(library);
      }
    });
  }
  
  deleteLibrary(libraryId: number){
    this.libraryService.deleteLibrary(libraryId).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.DELETE_LIBRARY_MESSAGE);
      this.router.navigate(['view-libraries']);
    });
  }
  
  onDeleteLibrary(library: LibraryI) {
    const modalRef: NgbModalRef = this.modalService.open(DeleteLibraryConfirmationComponent);
    modalRef.componentInstance.library = library;
    modalRef.result.then((result) => {
      if (result) {
        this.deleteLibrary(library.libraryId);
      }
    });
  }
  
  updateBookCopy(bookCopy: BookCopyI){
    this.bookCopyService.updateBookCopy(bookCopy).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_BOOK_COPY_MESSAGE);
      this.getBookCopyData();
    });    
  }
  
  onUpdateBookCopy(bookCopy: BookCopyI) {
    const modalRef: NgbModalRef = this.modalService.open(UpdateBookCopyConfirmationComponent);
    modalRef.componentInstance.updatedBook = bookCopy.book;
    modalRef.componentInstance.updatedBookCopy = bookCopy;
    modalRef.result.then((result) => {
      if (result) {
        this.updateBookCopy(bookCopy);
      }
    });
  }
  
  deleteBookCopy(id: number){
    this.bookCopyService.deleteBookCopy(id).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.DELETE_BOOK_COPY_MESSAGE);
      this.getBookCopyData();
    }); 
  }
  
  onDeleteBookCopy(bookCopy: BookCopyI) {
    const modalRef: NgbModalRef = this.modalService.open(DeleteBookCopyConfirmationComponent);
    modalRef.componentInstance.updatedBook = bookCopy.book;
    modalRef.componentInstance.bookCopyId = bookCopy.bookId;
    modalRef.result.then((result) => {
      if (result) {
        this.deleteBookCopy(bookCopy.bookId);
      }
    });
  }
  
  getLibraries() {
    this.libraryService.getAllLibraries().subscribe(result => this.libraries = result as LibraryI[]);
  }
  
  getPageData() {
    this.bookCopyService.getLibraryBookCopyCount(this.id).subscribe(result =>
      this.bookCopyCount = result as number)
  }

}
