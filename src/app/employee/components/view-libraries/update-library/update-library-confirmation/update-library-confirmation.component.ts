import {Component, OnInit} from '@angular/core';
import {LibraryI} from "../../../../../model/library.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-update-library-confirmation',
  templateUrl: './update-library-confirmation.component.html'
})
export class UpdateLibraryConfirmationComponent implements OnInit {
  library: LibraryI;
  updatedLibrary: LibraryI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  updateLibrary() {
    this.activeModal.close(true);
  }
}
