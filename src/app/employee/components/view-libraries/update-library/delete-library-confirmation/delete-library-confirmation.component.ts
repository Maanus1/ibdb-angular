import {Component, OnInit} from '@angular/core';
import {LibraryI} from "../../../../../model/library.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-library-confirmation',
  templateUrl: './delete-library-confirmation.component.html'
})
export class DeleteLibraryConfirmationComponent implements OnInit {
  library: LibraryI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  deleteLibrary() {
    this.activeModal.close(true);
  }
}
