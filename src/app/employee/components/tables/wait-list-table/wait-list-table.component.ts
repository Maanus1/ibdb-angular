import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BookWaitListItemI} from "../../../../model/book-wait-list-item.model";
import {Constants} from "../../../../constants";
import {PageInformation} from "../../../../model/page-information.model";
@Component({
  selector: 'app-wait-list-table',
  templateUrl: './wait-list-table.component.html'
})
export class WaitListTableComponent implements OnInit {
  @Input() waitList: BookWaitListItemI[];
  pageSizeOptions: number[];
  @Input() waitListItemCount: number;
  pageInformation: PageInformation = new PageInformation(1, 10);
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getDataEvent.emit(this.pageInformation);
  }
  
  trackByWaitListItem(index: number, waitListItem: BookWaitListItemI): number { return waitListItem.waitListId; }
    
  getWaitList(){
    this.getDataEvent.emit(this.pageInformation);
  }
  
  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getWaitList();
  }

  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getWaitList();
  }
}
