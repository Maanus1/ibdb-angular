import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {LibraryI} from "../../../../model/library.model";
import {Constants} from "../../../../constants";
import {PageInformation} from "../../../../model/page-information.model";

@Component({
  selector: 'app-library-table',
  templateUrl: './library-table.component.html'
})
export class LibraryTableComponent implements OnInit {
  @Input() libraries: LibraryI[];
  pageSizeOptions: number[];
  @Input() libraryCount: number;
  pageInformation: PageInformation = new PageInformation(1, 10);
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getLibraries();
  }
  
  trackByLibrary(index: number, library: LibraryI): number { return library.libraryId; }
  
  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getLibraries();
  }
  
  getLibraries(){
    this.getDataEvent.emit(this.pageInformation);
  }
  
  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getLibraries();
  }
}
