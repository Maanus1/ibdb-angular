import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BookI} from "../../../../model/book.model";
import {Constants} from "../../../../constants";
import {PageInformation} from "../../../../model/page-information.model";

@Component({
  selector: 'app-book-table',
  templateUrl: './book-table.component.html'
})
export class BookTableComponent implements OnInit {
  @Input() books: BookI[];
  pageSizeOptions: number[];
  @Input() bookCount: number;
  pageInformation: PageInformation = new PageInformation(1, 10);
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getDataEvent.emit(this.pageInformation);
  }

  trackByBook(index: number, book: BookI): string { return book.isbn; }
  
  getBooks(){
        console.log(123);
    
    this.getDataEvent.emit(this.pageInformation);
  }
    
  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getBooks();
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getBooks();
  }
}
