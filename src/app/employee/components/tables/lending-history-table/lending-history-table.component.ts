import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {LentBookI} from "../../../../model/lent-book.model";
import {Constants} from "../../../../constants";
import {PageInformation} from "../../../../model/page-information.model";

@Component({
  selector: 'app-lending-history-table',
  templateUrl: './lending-history-table.component.html'
})
export class LendingHistoryTableComponent implements OnInit {
  @Input() lentBooks: LentBookI[];
  pageSizeOptions: number[];
  lendingStatuses: Record<string, string>;
  @Input() lentBookCount: number;
  pageInformation: PageInformation = new PageInformation(1, 10);
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  @Output() cancelLendingEvent: EventEmitter<LentBookI> = new EventEmitter();
  @Output() updateLendingEvent: EventEmitter<LentBookI> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.lendingStatuses = Constants.BOOK_LENDING_STATUSES;
    this.getDataEvent.emit(this.pageInformation);
  }
    
  trackByLentBook(index: number, lentBook: LentBookI): number { return lentBook.id; }
  
  getLentBooks(){
    this.getDataEvent.emit(this.pageInformation);
  }
  
  cancelBookLending(lentBook: LentBookI){
    this.cancelLendingEvent.emit(lentBook);
  }
  
  updateLendingStatus(lentBook: LentBookI){
    this.updateLendingEvent.emit(lentBook);
  }
  
  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getLentBooks();
  }

  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getLentBooks();
  }
}
