import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {UserI} from "../../../../model/user.model";
import {Constants} from "../../../../constants";
import {PageInformation} from "../../../../model/page-information.model";

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html'
})
export class UserTableComponent implements OnInit {
  @Input() users: UserI[];
  pageSizeOptions: number[];
  @Input() userCount: number;
  pageInformation: PageInformation = new PageInformation(1, 10);
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getDataEvent.emit(this.pageInformation);
  }

  trackByUser(index: number, user: UserI): number { return user.userId; }
  
  
  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getUsers();
  }
  
  getUsers(){
    this.getDataEvent.emit(this.pageInformation);
  }
  
  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getUsers();
  }
}
