import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BookCopyI} from "../../../../model/book-copy.model";
import {LibraryI} from "../../../../model/library.model";
import {PageInformation} from "../../../../model/page-information.model";
import {LibraryService} from "../../../../service/model-service/library.service";
import {Constants} from "../../../../constants";

@Component({
  selector: 'app-book-copy-table',
  templateUrl: './book-copy-table.component.html'
})
export class BookCopyTableComponent implements OnInit {
  @Input() bookCopies: BookCopyI[];
  @Input() libraries: LibraryI[];
  @Input() bookCopyCount: number;
  @Input() bookPage: boolean;
  @Output() getDataEvent: EventEmitter<PageInformation> = new EventEmitter();
  @Output() deleteBookCopyEvent: EventEmitter<BookCopyI> = new EventEmitter();
  @Output() updateBookCopyEvent: EventEmitter<BookCopyI> = new EventEmitter();
  pageInformation: PageInformation = new PageInformation(1, 10);
  bookAvailable: boolean[] = [true, false];
  bookAvailabilityMap: Record<string, string>;
  pageSizeOptions: number[];
  constructor(private libraryService: LibraryService) { }

  ngOnInit() {
    this.bookAvailabilityMap = Constants.AVAILABILITY;
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getDataEvent.emit(this.pageInformation);
  }
  
  trackByBookCopy(index: number, bookCopy: BookCopyI): number { return bookCopy.bookId; }
  
  getBookCopies(){
    this.getDataEvent.emit(this.pageInformation);
  }
  
  getLibraryService() {
      return this.libraryService;
  } 
  
  deleteBookCopy(bookCopy: BookCopyI){
    this.deleteBookCopyEvent.emit(bookCopy);
  }
  
  updateBookCopy(bookCopy: BookCopyI){
    this.updateBookCopyEvent.emit(bookCopy);
  }
  
  changePage(pageNumber: number) {
    this.pageInformation.pageNumber = pageNumber;
    this.getBookCopies();
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageInformation.pageSize = pageSizeOption;
    this.getBookCopies();
  }
}
