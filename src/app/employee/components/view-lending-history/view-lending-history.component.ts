import {Component, OnInit, Input} from '@angular/core';
import {LentBookI} from "../../../model/lent-book.model";
import {BookI} from "../../../model/book.model";
import {UserI} from "../../../model/user.model";
import {PageInformationI} from "../../../model/page-information.model";
import {LentBookService} from "../../../service/model-service/lent-book.service";
import {UtilsService} from "../../../service/utils.service";
import {Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {NgbDateStruct, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from "../../../constants";
import {UpdateReservationStatusConfirmationComponent} from './update-reservation-status-confirmation/update-reservation-status-confirmation.component';
import {BookReservationCancellationConfirmationComponent} from '../../../shared/components/modal/book-reservation-cancellation-confirmation/book-reservation-cancellation-confirmation.component';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {LibraryService} from "../../../service/model-service/library.service";
import {LibraryI} from "../../../model/library.model";
import {NgbDatePipe} from "../../../shared/pipe/ngb-date/ngb-date.pipe";
import {AlertService} from "../../../service/alert.service";
import { collapseAnimation } from '../../../animations';
import { UserMessages } from '../../../user-messages';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-view-lending-history',
  templateUrl: './view-lending-history.component.html',
  animations: [collapseAnimation]
})
export class ViewLendingHistoryComponent implements OnInit {
  @Input() book: BookI;
  @Input() lentBook: LentBookI;
  @Input() user: UserI;
  lentBooks: LentBookI[];
  lendingStatuses: string[];
  lentBookCount: number;
  pageInformation: PageInformationI;
  lendingStatus: string = null;
  bookSearchPhrase: string = "";
  userSearchPhrase: string = "";
  fromDate: NgbDateStruct = null;
  toDate: NgbDateStruct = null;
  isCollapsed = true;
  library: LibraryI = null;
  libraries: LibraryI[];
  bookModel: Observable<BookI[]>;
  userModel: Observable<UserI[]>;
  searchingBook: boolean = false;
  searchingUser: boolean = false;
  bookLendingStatuses: Record<string, string>;
  faCaretDown = faCaretDown;
  constructor(private fb: FormBuilder, private router: Router, private lentBookService: LentBookService, private utilsService: UtilsService, private modalService: NgbModal, private libraryService: LibraryService, private alertService: AlertService, private ngbDatePipe: NgbDatePipe) {
  }

  ngOnInit() {
    this.bookLendingStatuses = Constants.BOOK_LENDING_STATUSES;
    this.getPageData();
    this.getAllLendingStatuses();
    this.getLibraries();
  }
  
  getLibraries() {
    this.libraryService.getAllLibraries().subscribe(result =>
      this.libraries = result as LibraryI[]);
  }

  filterLentBooks() {
    this.getPageData();
    this.getLentBooks(this.pageInformation);
  }

  searchBook = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.lentBookService.getUniqueLentBookBooks({
        pageNumber: 1,
        pageSize: 10,
        bookSearchPhrase: searchText,
        userSearchPhrase: this.userSearchPhrase,
        lendingStatus: this.lendingStatus,
        fromDate: this.ngbDatePipe.transform(this.fromDate),
        toDate: this.ngbDatePipe.transform(this.toDate),
        libraryId: (this.library ? this.library.libraryId : null)
      })),
    );
  };

  searchUser = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.lentBookService.getUniqueLentBookUsers({
        pageNumber: 1,
        pageSize: 10,
        bookSearchPhrase: this.bookSearchPhrase,
        userSearchPhrase: searchText,
        lendingStatus: this.lendingStatus,
        fromDate: this.ngbDatePipe.transform(this.fromDate),
        toDate: this.ngbDatePipe.transform(this.toDate),
        libraryId: (this.library ? this.library.libraryId : null)
      }))
    );
  };

  changeUserSearchPhrase(event) {
    this.userSearchPhrase = event.item ? event.item.email : event.target.value;
  }

  changeBookSearchPhrase(event) {
    this.bookSearchPhrase = event.item ? event.item.isbn : event.target.value;
  }

  bookResultFormatter(book) {
    return book.bookTitle + ', ' + book.bookAuthor + ',  ' +
      book.publisher + ', ' + book.publicationYear + ', ISBN: ' + book.isbn;
  }
  
  bookInputFormatter(book) {
    return book.bookTitle;
  }
  
  userResultFormatter(user) {
    return user.firstName + ' ' + user.lastName + ', ' + user.email;
  }
  
  userInputFormatter(user) {
    return user.email;
  }
  
  
  resetFilters() {
    this.fromDate = null;
    this.toDate = null;
    this.userModel = null;
    this.bookModel = null;
    this.bookSearchPhrase = "";
    this.userSearchPhrase = "";
    this.lendingStatus = null;
    this.library = null;
    this.filterLentBooks();
  }

  getPageData() {
    this.lentBookService.getLentBookCount({
      bookSearchPhrase: this.bookSearchPhrase,
      userSearchPhrase: this.userSearchPhrase,
      lendingStatus: this.lendingStatus,
      fromDate: this.ngbDatePipe.transform(this.fromDate),
      toDate: this.ngbDatePipe.transform(this.toDate),
      libraryId: (this.library ? this.library.libraryId : null)
    }).subscribe(result =>
      this.lentBookCount = result as number);
  }
  
  updateLendingStatus(id: number){
    this.lentBookService.updateLentBookStatus(id).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.UPDATE_LENDING_STATUS_MESSAGE);
      this.getAllLendingStatuses();
      this.getLentBooks(this.pageInformation);
    }); 
  }
  
  onUpdateLendingStatus(lentBook: LentBookI) {
    const modalRef: NgbModalRef = this.modalService.open(UpdateReservationStatusConfirmationComponent);
    modalRef.componentInstance.book = lentBook;
    modalRef.result.then((result) => {
      if (result) {
        this.updateLendingStatus(lentBook.id);
      }
    });
  }
  
  cancelBookLending(id: number){
    this.lentBookService.cancelBookLendingByAdmin(id).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.CANCEL_BOOK_LENDING_MESSAGE);
      this.getLentBooks(this.pageInformation);
    });
  }
  
  onCancelBookLending(lentBook: LentBookI) {
    const modalRef: NgbModalRef = this.modalService.open(BookReservationCancellationConfirmationComponent);
    modalRef.componentInstance.lentBook = lentBook;
    modalRef.componentInstance.user = lentBook.user;
    modalRef.result.then((result) => {
      if (result) {
        this.cancelBookLending(lentBook.id);
      }
    });
  }

  getAllLendingStatuses() {
    this.lentBookService.getAllLendingStatuses().subscribe(result => this.lendingStatuses = result as string[])
  }

  getLentBooks(pageInformation: PageInformationI) {
    this.pageInformation = pageInformation;
    this.lentBookService.getLentBooks({
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize,
      bookSearchPhrase: this.bookSearchPhrase,
      userSearchPhrase: this.userSearchPhrase,
      lendingStatus: this.lendingStatus,
      fromDate: this.ngbDatePipe.transform(this.fromDate),
      toDate: this.ngbDatePipe.transform(this.toDate),
      libraryId: (this.library ? this.library.libraryId : null)
    })
      .subscribe(result => this.lentBooks = result as LentBookI[]);
  }
}
