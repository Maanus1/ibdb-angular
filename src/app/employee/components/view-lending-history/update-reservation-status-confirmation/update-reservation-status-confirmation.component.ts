import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LentBookI} from "../../../../model/lent-book.model";

@Component({
  selector: 'app-update-reservation-status-confirmation',
  templateUrl: './update-reservation-status-confirmation.component.html'
})
export class UpdateReservationStatusConfirmationComponent implements OnInit {
  book: LentBookI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }


  updateLentBookStatus() {
    this.activeModal.close(true);
  }
}
