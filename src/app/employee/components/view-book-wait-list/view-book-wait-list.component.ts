import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {BookWaitListItemI} from "../../../model/book-wait-list-item.model";
import {LibraryI} from "../../../model/library.model";
import {PageInformation} from "../../../model/page-information.model";
import {BookWaitListItemService} from "../../../service/model-service/book-wait-list-item.service";
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {UtilsService} from "../../../service/utils.service";

@Component({
  selector: 'app-view-book-wait-list',
  templateUrl: './view-book-wait-list.component.html'
})
export class ViewBookWaitListComponent implements OnInit {
  waitList: BookWaitListItemI[];
  bookWaitListItem: Observable<BookWaitListItemI[]>;
  private waitListItemCount: number;
  pageInformation: PageInformation;
  pageSizeOptions: number[];
  searchPhrase: string = "";
  library: LibraryI;

  constructor(private bookWaitListItemService: BookWaitListItemService) {
  }

  ngOnInit() {
    this.getWaitListCount();
  }
    
  resetFilter() {
    this.searchPhrase = "";
    this.bookWaitListItem = undefined;
    this.getWaitList(this.pageInformation);
    this.getWaitListCount();
  }

  changeSearchPhrase(event) {
    this.searchPhrase = event.item ? event.item.book.isbn : event.target.value;
    this.getWaitList(this.pageInformation);
    this.getWaitListCount();
  }

  getWaitListCount() {
    this.bookWaitListItemService.getWaitListCount({
      searchPhrase: this.searchPhrase,
      libraryId: this.getLibraryId()
    }).subscribe(result => this.waitListItemCount = result as number);
  }

  resultFormatter(item) {
    return item.book.bookTitle + ', ' + item.book.bookAuthor + ',  ' + item.book.publisher + ', ' + item.book.publicationYear + ', ISBN: ' + item.book.isbn;
  }
  
  inputFormatter(item) {
    return item.book.bookTitle;
  }
  
  getLibraryId(){
    return this.library ? this.library.libraryId : null;
  }
  
  getWaitList(pageInformation: PageInformation) {
    this.pageInformation = pageInformation;
    this.bookWaitListItemService.getWaitList({
      pageNumber: pageInformation.pageNumber,
      pageSize: pageInformation.pageSize,
      searchPhrase: this.searchPhrase,
      libraryId: this.getLibraryId()
    })
      .subscribe(result => this.waitList = result as BookWaitListItemI[]);
  }

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.bookWaitListItemService.getWaitList({
        pageNumber: 1,
        pageSize: 10,
        searchPhrase: searchText,
        libraryId: this.getLibraryId()
      }))
    );
  };
}
