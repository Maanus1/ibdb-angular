import { Component, OnInit, OnChanges, Input, EventEmitter, Output } from '@angular/core';
import {Book, BookI} from "../../../../model/book.model";
import {ValidatorService} from "../../../../service/validator.service";
import { validFormAnimation, collapseAnimation2 } from '../../../../animations';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  animations: [validFormAnimation, collapseAnimation2]
})
export class BookFormComponent implements OnInit, OnChanges {
  bookForm: FormGroup;
  @Input() id: number;
  @Input() book: BookI;
  @Output() saveBookEvent: EventEmitter<BookI> = new EventEmitter();
  @Output() deleteBookEvent: EventEmitter<BookI> = new EventEmitter();
  
  constructor(private fb: FormBuilder, private validatorService: ValidatorService) { }

  ngOnInit() {
      this.bookForm = this.createBookForm();
  }
  
  ngOnChanges(changes){
    if(this.book){
      this.mapBookToForm(this.book);
    }
  }
  
  isDirty(): boolean{
    return this.bookForm.dirty;
  }

  saveBook(){
    let book: Book = this.mapFormToBook();
    this.saveBookEvent.emit(book);
  }
  
  deleteBook(){
    let book: Book = this.mapFormToBook();
    this.deleteBookEvent.emit(book);
  }
  
  createBookForm(){
    let bookForm: FormGroup = this.fb.group({
      isbn: ['', {updateOn: 'blur'}],
      bookTitle: ['', [Validators.required]],
      bookAuthor: ['', [Validators.required]],
      publicationYear: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      publisher: ['', [Validators.required]],
      imageUrl: ['', [Validators.required]]
    });
    if (!this.id) {
    	bookForm.controls['isbn'].setValidators([Validators.required]);
    	bookForm.controls['isbn'].setAsyncValidators([this.validatorService.isbnTaken]);
    }
    return bookForm;
  }
  
  mapBookToForm(book: BookI){
    this.bookForm.patchValue(
      {
        isbn: book.isbn,
        bookTitle: book.bookTitle,
        bookAuthor: book.bookAuthor,
        publicationYear: book.publicationYear,
        publisher: book.publisher,
        imageUrl: book.imageUrl
      })
  }
  
  mapFormToBook(): Book{
    let book = new Book();
    book.isbn = this.isbn.value;
    book.bookTitle = this.bookTitle.value;
    book.bookAuthor = this.bookAuthor.value;
    book.publicationYear = this.publicationYear.value;
    book.publisher = this.publisher.value;
    book.imageUrl = this.imageUrl.value;
    return book;
  }
  
  get isbn() {
    return this.bookForm.get('isbn')
  }

  get bookTitle() {
    return this.bookForm.get('bookTitle')
  }

  get bookAuthor() {
    return this.bookForm.get('bookAuthor')
  }

  get publicationYear() {
    return this.bookForm.get('publicationYear')
  }

  get publisher() {
    return this.bookForm.get('publisher')
  }
  
  get imageUrl() {
    return this.bookForm.get('imageUrl')
  }
}
