import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { validFormAnimation, collapseAnimation2 } from '../../../../animations';
import {NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Constants} from "../../../../constants";
import {Library, LibraryI} from "../../../../model/library.model";
import {UtilsService} from "../../../../service/utils.service";

@Component({
  selector: 'app-library-form',
  templateUrl: './library-form.component.html',
  animations: [validFormAnimation, collapseAnimation2]
})
export class LibraryFormComponent implements OnInit, OnChanges {
  @Input() library: LibraryI;
  @Input() id: number;
  @Input() isAdmin: boolean;
  @Output() saveLibraryEvent: EventEmitter<Library> = new EventEmitter();
  @Output() deleteLibraryEvent: EventEmitter<Library> = new EventEmitter();
  libraryForm: FormGroup;
  
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.libraryForm = this.createLibraryForm();
    if (!this.isAdmin){
    	this.libraryForm.disable();
    }
  }
  
  isDirty(): boolean{
    return this.libraryForm.dirty;
  }
  
  ngOnChanges(changes){
    if(this.library){
      this.mapLibraryToForm(this.library);
    }
  }
  
  saveLibrary(){
    const library: Library = this.mapFormToLibrary();
    this.saveLibraryEvent.emit(library);
  }
  
  deleteLibrary(){
    const library: Library = this.mapFormToLibrary();
    this.deleteLibraryEvent.emit(library);
  }
  
  createLibraryForm(): FormGroup{
    return this.fb.group({
      libraryId: [''],
      address: ['', [Validators.required, Validators.maxLength(50)]],
      latitude: ['', [Validators.required, Validators.pattern(Constants.REGEX_PATTERN_DECIMAL)]],
      longitude: ['', [Validators.required, Validators.pattern(Constants.REGEX_PATTERN_DECIMAL)]],
      openingTime: ['', [Validators.required]],
      closingTime: ['', [Validators.required]]
    });
  }
  
  mapLibraryToForm(library: LibraryI){
    this.libraryForm.patchValue(
      {
        libraryId: library.libraryId,
        address: library.address,
        latitude: library.latitude,
        longitude: library.longitude,
        openingTime:UtilsService.stringToNgbTimeStruct(library.openingTime),
        closingTime: UtilsService.stringToNgbTimeStruct(library.closingTime)
      });
  }
  
  
  mapFormToLibrary(): Library{
    let library = new Library();
    library.libraryId = this.libraryId.value;
    library.address = this.address.value;
    library.latitude = this.latitude.value;
    library.longitude = this.longitude.value;
    library.openingTime = UtilsService.ngbTimeStructToString(this.openingTime.value);
    library.closingTime = UtilsService.ngbTimeStructToString(this.closingTime.value);
    return library;
  }
  
  
  get libraryId() {
    return this.libraryForm.get('libraryId')
  }

  get address() {
    return this.libraryForm.get('address')
  }

  get latitude() {
    return this.libraryForm.get('latitude')
  }

  get longitude() {
    return this.libraryForm.get('longitude')
  }
  
  get openingTime() {
    return this.libraryForm.get('openingTime')
  }
  
  get closingTime() {
    return this.libraryForm.get('closingTime')
  }
}
