import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ViewBooksComponent} from "./components/view-books/view-books.component";
import {ViewUsersComponent} from "./components/view-users/view-users.component";
import {ViewLendingHistoryComponent} from "./components/view-lending-history/view-lending-history.component";
import {UpdateBookComponent} from "./components/view-books/update-book/update-book.component";
import {UpdateUserComponent} from "./components/view-users/update-user/update-user.component";
import {AuthGuardService as AuthGuard} from '../service/routing/auth-guard.service';
import {RoleGuardService as RoleGuard} from '../service/routing/role-guard.service';
import {DeleteUserConfirmationComponent} from "./components/view-users/update-user/delete-user-confirmation/delete-user-confirmation.component";
import {UpdateUserConfirmationComponent} from "./components/view-users/update-user/update-user-confirmation/update-user-confirmation.component";
import {BookReservationCancellationConfirmationComponent} from "../shared/components/modal/book-reservation-cancellation-confirmation/book-reservation-cancellation-confirmation.component";
import {UpdateBookConfirmationComponent} from "./components/view-books/update-book/update-book-confirmation/update-book-confirmation.component";
import {DeleteBookConfirmationComponent} from "./components/view-books/update-book/delete-book-confirmation/delete-book-confirmation.component";
import {DeleteBookCopyConfirmationComponent} from "./components/modal/delete-book-copy-confirmation/delete-book-copy-confirmation.component";
import {UpdateBookCopyConfirmationComponent} from "./components/modal/update-book-copy-confirmation/update-book-copy-confirmation.component";
import {ViewLibrariesComponent} from './components/view-libraries/view-libraries.component';
import {UpdateLibraryComponent} from './components/view-libraries/update-library/update-library.component';
import {UpdateLibraryConfirmationComponent} from './components/view-libraries/update-library/update-library-confirmation/update-library-confirmation.component';
import {DeleteLibraryConfirmationComponent} from './components/view-libraries/update-library/delete-library-confirmation/delete-library-confirmation.component';
import {ViewBookWaitListComponent} from './components/view-book-wait-list/view-book-wait-list.component';
import {CanDeactivateGuardService as CanDeactivateGuard} from "../service/routing/can-deactivate-guard.service";
import { DataUpdateCancellationConfirmationComponent } from './components/modal/data-update-cancellation-confirmation/data-update-cancellation-confirmation.component';
import {UpdateReservationStatusConfirmationComponent} from './components/view-lending-history/update-reservation-status-confirmation/update-reservation-status-confirmation.component';

const routes: Routes = [
  {path: '',
   children: [
   {path: '',
    canActivate: [RoleGuard],
    data: {expectedRole: ['ROLE_ADMIN']},
    children: [
      {path: 'view-users', 
       children: [
         {path: '', component: ViewUsersComponent},
         {path: 'add-user', component: UpdateUserComponent, canDeactivate: [CanDeactivateGuard]},
	     {path: 'update-user/:id',
	      children: [
	        {path: '', component: UpdateUserComponent, canDeactivate: [CanDeactivateGuard]},
	        {path: 'data-update-cancellation-confirmation', component: DataUpdateCancellationConfirmationComponent},
	        {path: 'delete-user-confirmation', component: DeleteUserConfirmationComponent},
	        {path: 'update-user-confirmation', component: UpdateUserConfirmationComponent},
	     ]}
       ]}
   ]},
  {path: '',
   canActivate: [RoleGuard],
   data: {expectedRole: ['ROLE_EMPLOYEE']},
   children:[
     {path: 'view-books',
      children: [
        {path: '', component: ViewBooksComponent},
        {path: 'add-book', component: UpdateBookComponent, canDeactivate: [CanDeactivateGuard]},
        {path: 'update-book/:id',
         children: [
           {path: '', component: UpdateBookComponent, canDeactivate: [CanDeactivateGuard]},
           {path: 'data-update-cancellation-confirmation', component: DataUpdateCancellationConfirmationComponent},          
           {path: 'update-book-confirmation', component: UpdateBookConfirmationComponent},
     	   {path: 'delete-book-confirmation', component: DeleteBookConfirmationComponent},
     	   {path: 'delete-book-copy-confirmation', component: DeleteBookCopyConfirmationComponent},
     	   {path: 'update-book-copy-confirmation', component: UpdateBookCopyConfirmationComponent}
         ]},
      ]}, 
     {path: 'view-libraries', 
	   children: [
	     {path: '', component: ViewLibrariesComponent},
	     {path: 'add-library', component: UpdateLibraryComponent, canDeactivate: [CanDeactivateGuard]},
	     {path: 'update-library/:id', 
	      children: [
	        {path: '', component: UpdateLibraryComponent, canDeactivate: [CanDeactivateGuard]},
	        {path: 'data-update-cancellation-confirmation', component: DataUpdateCancellationConfirmationComponent},          
	        {path: 'update-library-confirmation', component: UpdateLibraryConfirmationComponent},
	        {path: 'delete-library-confirmation', component: DeleteLibraryConfirmationComponent},
	        {path: 'delete-book-copy-confirmation', component: DeleteBookCopyConfirmationComponent},
            {path: 'update-book-copy-confirmation', component: UpdateBookCopyConfirmationComponent}       
	     ]}
	  ]},
     {path: 'view-lending-history',
      children: [
      	{path: '', component: ViewLendingHistoryComponent},
      	{path: 'update-reservation-status-confirmation',component: UpdateReservationStatusConfirmationComponent},
      	{path: 'book-reservation-cancellation-confirmation', component: BookReservationCancellationConfirmationComponent}
      ]},
     {path: 'view-book-wait-list', component: ViewBookWaitListComponent},
   ]}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule {
}
