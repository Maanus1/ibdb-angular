export class UserMessages {
  public static readonly DELETE_BOOK_MESSAGE: string = 'Successfully deleted book!';
  
  public static readonly UPDATE_BOOK_MESSAGE: string = 'Successfully updated book!';
  
  public static readonly ADD_BOOK_MESSAGE: string = 'Successfully added book!';
  
  public static readonly ADD_BOOK_COPY_MESSAGE: string = 'Successfully added book copy!';
  
  public static readonly DELETE_BOOK_COPY_MESSAGE: string = 'Successfully deleted book copy!';
  
  public static readonly UPDATE_BOOK_COPY_MESSAGE: string = 'Successfully updated book copy!';
  
  public static readonly UPDATE_LENDING_STATUS_MESSAGE: string = 'Successfully updated lending status!';
  
  public static readonly CANCEL_BOOK_LENDING_MESSAGE: string = 'Successfully cancelled book reservation!';
  
  public static readonly ADD_LIBRARY_MESSAGE: string = 'Successfully added library!';
  
  public static readonly UPDATE_LIBRARY_MESSAGE: string = 'Successfully updated library!';
  
  public static readonly DELETE_LIBRARY_MESSAGE: string = 'Successfully deleted library!';
  
  public static readonly UPDATE_USER_MESSAGE: string = 'Successfully updated user!';
  
  public static readonly DELETE_USER_MESSAGE: string = 'Successfully deleted user!';
  
  public static readonly ADD_USER_MESSAGE: string = 'Successfully added user!';
  
  public static readonly UPDATE_USER_INFORMATION_MESSAGE: string = 'Successfully changed user information';
  
  public static readonly CHANGE_PASSWORD_MESSAGE: string = 'Password successfully changed';
  
  public static readonly DELETE_RATING_MESSAGE: string = 'Successfully deleted rating!';
  
  public static readonly DELETE_WAIT_LIST_ITEM_MESSAGE: string = 'Successfully deleted book from wait list!';
  
  public static readonly ADD_BOOK_RESERVATION_MESSAGE: string = 'Successfully added book reservation!';
  
  public static readonly ADD_BOOK_COMMENT_MESSAGE: string = 'Successfully added comment!';
  
  public static readonly DELETE_BOOK_COMMENT_MESSAGE: string = 'Successfully deleted comment!';
  
  public static readonly EDIT_BOOK_COMMENT_MESSAGE: string = 'Successfully edited comment!';
}