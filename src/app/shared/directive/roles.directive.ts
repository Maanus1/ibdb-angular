import {Directive, Input, OnChanges, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from '../../service/authorization/auth.service';

@Directive({
  selector: '[roles]'
})
export class RolesDirective implements OnChanges{
  @Input('roles') rolesAllowed: string[];
  @Input('rolesLoggedIn') loggedIn: boolean;
  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private authService: AuthService) {

  }
  
  ngOnChanges(){
    if(this.rolesAllowed && this.loggedIn !== undefined){
      let shouldShow: boolean = false;
      shouldShow = this.authService.hasAnyRole(this.rolesAllowed);   
      if (shouldShow) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    }
  }
}
