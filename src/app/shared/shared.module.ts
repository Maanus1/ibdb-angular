import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {AgmCoreModule} from '@agm/core';
import { jwtTokenGetter } from '../service/authorization/jwt-helper-function';
import { environment } from '../../environments/environment';
import {JwtModule} from "@auth0/angular-jwt";
import { UserFormComponent } from './components/forms/user-form/user-form.component';
import { PasswordFormComponent } from './components/forms/password-form/password-form.component';
import { InputValidationErrorComponent } from './components/input-validation-error/input-validation-error.component';
import {LoginComponent} from './components/login/login.component';
import {HeaderComponent} from './components/header/header.component';
import {IndexComponent} from './components/index/index.component';
import {AlertComponent} from './components/alert/alert.component';
import { ShortenStringPipe } from './pipe/shorten-string/shorten-string.pipe';
import {AppRoutingModule} from "../app-routing.module";
import {BookReservationCancellationConfirmationComponent} from './components/modal/book-reservation-cancellation-confirmation/book-reservation-cancellation-confirmation.component';
import { DateStringPipe } from './pipe/date-string/date-string.pipe';
import { NgbDatePipe } from './pipe/ngb-date/ngb-date.pipe';
import {RolesDirective} from './directive/roles.directive';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    UserFormComponent,
    PasswordFormComponent,
    InputValidationErrorComponent,
    LoginComponent,
    HeaderComponent,
    IndexComponent,
    AlertComponent,
    ShortenStringPipe,
    BookReservationCancellationConfirmationComponent,
    DateStringPipe,
    NgbDatePipe,
    RolesDirective,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapsAPIKey
    }),
    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter
      }
    })
  ],
  exports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgbModule,
    AgmCoreModule,
    JwtModule,
    UserFormComponent,
    PasswordFormComponent,
    InputValidationErrorComponent,
    LoginComponent,
    HeaderComponent,
    IndexComponent,
    AlertComponent,   
    ShortenStringPipe,    
    BookReservationCancellationConfirmationComponent,
    DateStringPipe,
    NgbDatePipe,
    RolesDirective,
    PageNotFoundComponent   
  ]
})
export class SharedModule { }
