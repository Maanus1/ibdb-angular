import { Pipe, PipeTransform } from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from '../../../constants';

@Pipe({
  name: 'ngbDate'
})
export class NgbDatePipe implements PipeTransform {

  transform(date: NgbDateStruct): string {
    let separator = Constants.DATE_FORMAT_SEPARATOR;
    if (date == undefined || date == null || typeof date == 'string') {
      return "";
    }
    let str = "";
    if (date.day.toString().length == 1) {
      str = str + "0" + date.day + separator;
    }
    else {
      str = str + date.day + separator;
    }
    if (date.month.toString().length == 1) {
      str = str + "0" + date.month + separator;
    }
    else {
      str = str + date.month + separator;
    }
    str = str + date.year;
    return str;
  }

}
