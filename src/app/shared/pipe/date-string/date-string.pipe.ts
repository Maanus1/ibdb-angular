import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from '../../../constants';

@Pipe({
  name: 'dateString'
})
export class DateStringPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      const dateParts = value.trim().split(Constants.DATE_FORMAT_SEPARATOR);
      if (dateParts.length === 1) {
        return new Date(null,null,dateParts[0]);;
      } else if (dateParts.length === 2) {
        return new Date(null,dateParts[1] - 1,dateParts[0]);
      } else if (dateParts.length === 3) {
        return new Date(dateParts[2],dateParts[1] - 1,dateParts[0]);
      }
    }
    return null;
  }

}
