import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenStringPipe implements PipeTransform {

  transform(text: string, start: number, end: number): any {
    if (text == null) {
    	return text;
    }
    if (text.length > end){
        return text.substring(start, end) + '..';
    }
    return text.substring(start, end);
  }

}
