import {Component, OnInit} from '@angular/core';
import {LentBookI} from "../../../../model/lent-book.model";
import {UserI} from "../../../../model/user.model";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-book-reservation-cancellation-confirmation',
  templateUrl: './book-reservation-cancellation-confirmation.component.html'
})
export class BookReservationCancellationConfirmationComponent implements OnInit {
  lentBook: LentBookI;
  user: UserI;

  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  cancelBookLending() {
    this.activeModal.close(true);
  }
}
