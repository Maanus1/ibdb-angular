import { Component, OnInit, Input } from '@angular/core';
import {faExclamation} from '@fortawesome/free-solid-svg-icons';
import { collapseAnimation2 } from '../../../animations';

@Component({
  selector: 'app-input-validation-error',
  templateUrl: './input-validation-error.component.html',
  styleUrls: ['./input-validation-error.component.css'],
  animations: [ collapseAnimation2 ]
})
export class InputValidationErrorComponent implements OnInit {
  @Input() condition: boolean;
  faExclamation = faExclamation;
  constructor() { }

  ngOnInit() {
  }
}
