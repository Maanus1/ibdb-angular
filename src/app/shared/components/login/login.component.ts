import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../../../service/authorization/auth.service";
import {HttpParams} from "@angular/common/http";
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilsService} from "../../../service/utils.service";
import { validFormAnimation, collapseAnimation2 } from '../../../animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  animations: [validFormAnimation, collapseAnimation2]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  alertMessage: string;
  alert: boolean = false;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService, private utilsService: UtilsService, private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
    window.sessionStorage.removeItem('token');
    this.loginForm = this.fb.group({
      email: ['', [Validators.required,Validators.email]],
      password: ['', Validators.required]
    });
  }

  displayMessage(message: string) {
    this.alertMessage = message;
    this.alert = true;
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.email.value, this.password.value).subscribe(() => {
      this.activeModal.close();
    });
  }

  close() {
    this.activeModal.close();
  }

  createAccount() {
    this.activeModal.close();
    this.router.navigate(['/create-account']);
  }
  
  get email() {
    return this.loginForm.get('email')
  }
  
  get password() {
    return this.loginForm.get('password')
  }
}
