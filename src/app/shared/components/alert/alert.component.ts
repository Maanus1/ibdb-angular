import {Component, OnInit, TemplateRef} from '@angular/core';
import {AlertService} from '../../../service/alert.service'
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(500, style({ opacity: 1 }))
      ]),
      transition(':leave', [
        animate(500, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class AlertComponent implements OnInit {

  constructor(public alertService: AlertService) {
  }

  ngOnInit() {
  }

  isTemplate(toast) {
    return toast.textOrTpl instanceof TemplateRef;
  }

}
