import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { validFormAnimation, collapseAnimation2 } from '../../../../animations';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ValidatorService} from "../../../../service/validator.service";

@Component({
  selector: 'app-password-form',
  templateUrl: './password-form.component.html',
  animations: [validFormAnimation, collapseAnimation2]
})
export class PasswordFormComponent implements OnInit {
  passwordForm: FormGroup;
  @Output() changePasswordEvent: EventEmitter<Object> = new EventEmitter()
  constructor( private fb: FormBuilder, private validatorService: ValidatorService) { }

  ngOnInit() {
    this.passwordForm = this.createPasswordForm();
  }
  
  resetForm(){
    this.passwordForm.reset();
  }
  
  createPasswordForm(): FormGroup{
    return this.fb.group({
      currentPassword: ['', [Validators.required]],
      password: ['', [Validators.required]],
      confirmPassword: ['', [Validators.required]],
    }, {validator: this.validatorService.passwordConfirming});
  }
  
  changePassword(){
    const currentPassword = this.currentPassword.value;
    const newPassword = this.password.value;
    this.changePasswordEvent.emit({currentPassword: currentPassword, newPassword: newPassword});
  }
  
  get currentPassword() {
    return this.passwordForm.get('currentPassword')
  }

  get password() {
    return this.passwordForm.get('password')
  }

  get confirmPassword() {
    return this.passwordForm.get('confirmPassword')
  }
}
