import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormArray} from "@angular/forms";
import {ValidatorService} from "../../../../service/validator.service";
import {UtilsService} from "../../../../service/utils.service";
import {AuthService} from "../../../../service/authorization/auth.service";
import { validFormAnimation, collapseAnimation2 } from '../../../../animations';
import {NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import {User, UserI} from "../../../../model/user.model";
import {RoleI} from "../../../../model/role.model";
import {LibraryI} from "../../../../model/library.model";
import {faCheck} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  animations: [validFormAnimation, collapseAnimation2]
})
export class UserFormComponent implements OnInit, OnChanges {
  @Input() user: UserI;
  @Input() id: number;
  @Input() roleList: RoleI[];
  @Input() libraries: LibraryI[];
  @Input() adminForm: boolean;
  @Input() createAccountForm: boolean;
  @Input() userProfileForm: boolean;
  @Output() saveUserEvent: EventEmitter<User> = new EventEmitter();
  @Output() deleteUserEvent: EventEmitter<UserI> = new EventEmitter();
  currentUser: string;
  userForm: FormGroup;
  monthList: number[];
  yearList: number[];
  dayList: number[];
  faCheck = faCheck;
  constructor(private fb: FormBuilder, private validatorService: ValidatorService, private calendar: NgbCalendar, private authService: AuthService) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
    this.userForm = this.createUserForm(); 
    this.generateMonthList();
    this.generateYearList();
    this.generateDayList();
  }
  
 ngOnChanges(changes) {
  if(this.adminForm && this.roleList){
    this.roleList.forEach(role => {
    this.roles.push(this.fb.control(''));
    });
  }
  if(this.user){
    this.addUserToForm(this.user);
  }
}

  isDirty(): boolean{
    return this.userForm.dirty;
  }
  
   daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }
  
  generateMonthList(){
    this.monthList=this.calendar.getMonths();
    this.userForm.get('birthday.month').setValue(this.monthList[0]);
  }
  
  generateDayList(){
    let dayList: number[] = [];
    let year = this.userForm.get('birthday.year').value;
    let month = this.userForm.get('birthday.month').value;
    let dayCount = this.daysInMonth(month,year);
    for (let i = 1; i<dayCount+1; i++){
      dayList.push(i);
    }
    this.dayList = dayList;
    if(!this.userForm.get('birthday.day').value){
      this.userForm.get('birthday.day').setValue(this.dayList[0]);
    }
    }
   

  generateYearList(){
    let yearList: number[] = [];
    const thisYear = this.calendar.getToday().year;
    for (let i = 0; i<=110; i++){
      yearList.push(thisYear-i);
    }
    this.yearList = yearList;
    this.userForm.get('birthday.year').setValue(this.yearList[0]);
  }
  
  mapFormToUser(): User{
    let user: User = new User();
    user.userId=this.id;
    user.email = this.email.value;
    user.alias = this.alias.value;
    user.password = this.password.value;
    user.firstName = this.firstName.value;
    user.lastName = this.lastName.value;
    user.preferredLibrary = this.libraries.find(x => x.libraryId==this.preferredLibrary.value);
    let date = this.birthday.value;
    user.birthday = UtilsService.convertDateToString(new Date(date.year,date.month,date.day));
    if (this.adminForm){
      this.roles.controls.forEach((item, index) => {
      this.roleList[index].checked = item.value
      });
      user.roles = this.roleList.filter(x => x.checked==true);
    }
    return user;
  }
  
  saveUser(){
    const user: User = this.mapFormToUser();
    this.saveUserEvent.emit(user);
  }
  
  createUserForm(): FormGroup{
    let userForm =  this.fb.group({
      email: [{value: '', disabled: this.userProfileForm || (this.id && this.currentUser == this.id.toString())}, {updateOn: 'blur'}],
      alias: [{value: '', disabled: this.userProfileForm}, {updateOn: 'blur'}],
      password: [''],
      confirmPassword: [''],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      birthday: this.fb.group({
         day: ['',[Validators.required]],
         month: ['',[Validators.required]],
         year: ['',[Validators.required]]
      }),
      preferredLibrary: [null],
      roles: this.fb.array([])
    });
    
    if (this.adminForm || this.createAccountForm) {
        userForm.controls['alias'].setAsyncValidators([this.validatorService.aliasTaken(this.id)]);
        userForm.controls['alias'].setValidators([Validators.required]);
        userForm.controls['email'].setValidators([Validators.required, Validators.email] );
        userForm.controls['email'].setAsyncValidators([this.validatorService.emailTaken(this.id)]);
        if(!this.id){
    	  userForm.controls['password'].setValidators([Validators.required]);
    	}
    }
    if(this.createAccountForm){
      userForm.controls['confirmPassword'].setValidators([Validators.required]);
      userForm.setValidators([this.validatorService.passwordConfirming]);
    }
    return userForm;
  }
  
  addUserToForm(user: UserI){
    if(!user){
      return;
    }
    let birthday: Date = UtilsService.convertDateStringToDate(user.birthday);
    this.userForm.patchValue(
      {
        email: user.email,
        alias: user.alias,
        firstName: user.firstName,
        lastName: user.lastName,
        preferredLibrary: (user.preferredLibrary ? user.preferredLibrary.libraryId : null),
        birthday: { day: birthday.getDate(), month:birthday.getMonth(),year:birthday.getFullYear()}
      })
    if(this.adminForm){
      this.roleList.forEach((role,index) => {
        this.roles.at(index).patchValue(this.user.roles.some(x => x.name === role.name));
      });
    }
  }
  
  deleteUser(){
    this.deleteUserEvent.emit(this.user);
  }
  
  get email() {
    return this.userForm.get('email')
  }
  
  get alias() {
    return this.userForm.get('alias')
  }
  
  get password() {
    return this.userForm.get('password')
  }
  
  get confirmPassword() {
    return this.userForm.get('confirmPassword')
  }
  
  get firstName() {
    return this.userForm.get('firstName')
  }

  get lastName() {
    return this.userForm.get('lastName')
  }
  
  get preferredLibrary() {
    return this.userForm.get('preferredLibrary')
  }
  
  get birthday() {
    return this.userForm.get('birthday')
  }
  
  get day() {
    return this.userForm.get('day')
  }
  
  get month() {
    return this.userForm.get('month')
  }
  
  get year() {
    return this.userForm.get('year')
  }
  
  get roles() {
    return this.userForm.get('roles') as FormArray
  }
}
