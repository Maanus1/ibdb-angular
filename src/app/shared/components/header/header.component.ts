import {Component, OnInit, OnDestroy} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginComponent} from "../login/login.component";
import {Notification} from "../../../model/notification.model";
import {NotificationService} from "../../../service/model-service/notification.service";
import {Constants} from "../../../constants";
import {AuthService} from "../../../service/authorization/auth.service";
import {interval, Subscription} from 'rxjs';
import {faEnvelope} from '@fortawesome/free-solid-svg-icons';
import { trigger, state, style, animate, transition, query, animateChild, group } from '@angular/animations';
import {Router} from "@angular/router";
import {backgroundColorAnimation} from "../../../animations";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [backgroundColorAnimation]
})
export class HeaderComponent implements OnInit, OnDestroy {
  public notifications: Notification[];
  public notificationCount: number = 0;
  notificationsShown: number = 5;
  faEnvelope = faEnvelope;
  asAdmin: boolean = false;
  language: string;
  languages: Object;
  loggedIn: boolean;
  userSubscription: Subscription;
  notificationInterval: Subscription;
  constructor(private modalService: NgbModal, private notificationService: NotificationService, private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.languages = Constants.LANGUAGES;
    this.language = !!localStorage.getItem(Constants.LANGUAGE) ? localStorage.getItem(Constants.LANGUAGE) : 'et_EE';
    this.userSubscription = this.authService.loggedIn.subscribe(
      result => {this.loggedIn = result as boolean;
      this.asAdmin = this.authService.hasAnyRole(['ROLE_EMPLOYEE','ROLE_ADMIN']);
      }
    )
    if (this.loggedIn) {
      this.getNotifications();
      this.getNotificationCount();
    }
    this.notificationInterval = interval(5000).subscribe(() => {
      if (this.loggedIn) {
        this.getNotifications();
        this.getNotificationCount();
      }
    });
  }
  
  ngOnDestroy(){
    this.userSubscription.unsubscribe();
    this.notificationInterval.unsubscribe();
  }
  
  
  changeLanguage(language){
    localStorage.setItem(Constants.LANGUAGE,language);
    this.language=language;
    window.location.reload();
  }
  
  trackByLentBook(index: number, notification: Notification): number { return notification.notificationId; }
  
  markNotificationsAsSeen(wasOpened) {
    if (wasOpened == false) {
      this.notificationService.markNotificationsAsSeen().subscribe(() => {
        this.getNotifications();
        this.getNotificationCount();
      });
    }
  }
  
  navigateToRoute(notification){
  	let notificationType = notification.notificationType;
  	if (notificationType == 'USER_INFORMATION'){
  		this.router.navigate(['/user-profile/account']);
  	}
  	else if (notificationType == 'LENDING_ACTIVATED' || notificationType == 'RESERVATION_CANCELLED' || notificationType == 'BOOK_RETURNED'){
  		this.router.navigate(['/user-profile/lendings']);
  	}
  	else if (notificationType == 'BOOK_AVAILABLE' || notificationType == 'COMMENT_REPLY'){
  		this.router.navigate(['/view-ratings/book/', notification.book.isbn]);
  	}
  	
  }
  
  getNotifications() {
    this.notificationService.getNotifications({
      pageNumber: 1,
      pageSize: 5
    }).subscribe(result => this.notifications = result as Notification[]);
  }

  getNotificationCount() {
    this.notificationService.getUnreadNotificationCount().subscribe(result => this.notificationCount = result as number);
  }

  logout() {
    this.authService.logout();
  }

  open() {
    this.modalService.open(LoginComponent);
  }
}
