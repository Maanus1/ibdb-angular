import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ViewRatingsComponent} from "./components/view-ratings/view-ratings.component";
import {ViewBookRatingComponent} from "./components/view-ratings/view-book-rating/view-book-rating.component";
import {UserProfileComponent} from "./components/user-profile/user-profile.component";
import {UserInformationComponent} from "./components/user-profile/user-information/user-information.component";
import {UserRatingsComponent} from "./components/user-profile/user-ratings/user-ratings.component";
import {UserLendingHistoryComponent} from "./components/user-profile/user-lending-history/user-lending-history.component";
import {UserNotificationsComponent} from "./components/user-profile/user-notifications/user-notifications.component";
import {UserWaitListComponent} from "./components/user-profile/user-wait-list/user-wait-list.component";
import {CreateAccountComponent} from "./components/create-account/create-account.component";
import {UserInformationChangeConfirmationComponent} from './components/user-profile/user-information/user-information-change-confirmation/user-information-change-confirmation.component';
import {ViewLibraryLocationsComponent} from './components/view-library-locations/view-library-locations.component';
import { BookCommentsComponent } from './components/view-ratings/view-book-rating/book-comments/book-comments.component';
import {AuthGuardService as AuthGuard} from '../service/routing/auth-guard.service';
import {PasswordChangeConfirmationComponent} from './components/user-profile/user-information/password-change-confirmation/password-change-confirmation.component';
import {BookReservationCancellationConfirmationComponent} from "../shared/components/modal/book-reservation-cancellation-confirmation/book-reservation-cancellation-confirmation.component";

const routes: Routes = [
  {path: 'view-ratings', redirectTo: 'view-ratings/all'},
  {path: 'view-ratings/:id', children:[
    {path: '', component: ViewRatingsComponent},
    {path: 'book/:isbn', children:[
    	{path: '', component: ViewBookRatingComponent},
    	{path: 'view-comments', component: BookCommentsComponent}
    ]}
  ]},
  {path: 'view-library-locations', component: ViewLibraryLocationsComponent},
  {path: 'create-account', component: CreateAccountComponent},
  {path: '',
   canActivate: [AuthGuard],
   children: [
     {path: 'user-profile', component: UserProfileComponent, children:[
       {path: '', redirectTo: 'account',  pathMatch: 'full'},
       {path: 'account', component: UserInformationComponent, children:[
         {path: 'password-change-confirmation', component: PasswordChangeConfirmationComponent},
         {path: 'user-information-change-confirmation', component: UserInformationChangeConfirmationComponent}
       ]},
       {path: 'ratings', component: UserRatingsComponent},
       {path: 'lendings', component: UserLendingHistoryComponent, children:[
         {path: 'book-reservation-cancellation-confirmation', component: BookReservationCancellationConfirmationComponent}
       ]},
       {path: 'notifications', component: UserNotificationsComponent},
       {path: 'wait-list', component: UserWaitListComponent},
     ]},    
   ]},
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {
}
