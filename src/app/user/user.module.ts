import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ViewRatingsComponent} from './components/view-ratings/view-ratings.component';
import {ViewBookRatingComponent} from './components/view-ratings/view-book-rating/view-book-rating.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {CreateAccountComponent} from './components/create-account/create-account.component';
import {UserInformationChangeConfirmationComponent} from './components/user-profile/user-information/user-information-change-confirmation/user-information-change-confirmation.component';
import {ViewLibraryLocationsComponent} from './components/view-library-locations/view-library-locations.component';
import {UserInformationComponent} from './components/user-profile/user-information/user-information.component';
import {UserLendingHistoryComponent} from './components/user-profile/user-lending-history/user-lending-history.component';
import {UserRatingsComponent} from './components/user-profile/user-ratings/user-ratings.component';
import {UserWaitListComponent} from './components/user-profile/user-wait-list/user-wait-list.component';
import {UserNotificationsComponent} from './components/user-profile/user-notifications/user-notifications.component';
import {BookInformationCardComponent} from './components/book-card/book-information-card/book-information-card.component';
import { BookCommentsComponent } from './components/view-ratings/view-book-rating/book-comments/book-comments.component';
import { TreeViewComponent } from './components/view-ratings/view-book-rating/book-comments/tree-view/tree-view.component';
import { BookCommentComponent } from './components/view-ratings/view-book-rating/book-comments/tree-view/book-comment/book-comment.component';
import { LendingDatePickerComponent } from './components/view-ratings/view-book-rating/lending-date-picker/lending-date-picker.component';
import { BookUserProfileCardComponent } from './components/book-card/book-user-profile-card/book-user-profile-card.component';
import { SharedModule } from '../shared/shared.module';
import {UserRoutingModule} from "./user-routing.module";
import {PasswordChangeConfirmationComponent} from './components/user-profile/user-information/password-change-confirmation/password-change-confirmation.component';

@NgModule({
  declarations: [
    ViewRatingsComponent,
    ViewBookRatingComponent,
    UserProfileComponent,
    CreateAccountComponent,
    UserInformationChangeConfirmationComponent,
    ViewLibraryLocationsComponent,
    UserInformationComponent,
    UserLendingHistoryComponent,
    UserRatingsComponent,
    UserWaitListComponent,
    UserNotificationsComponent,
    BookInformationCardComponent,
    BookCommentsComponent,
    TreeViewComponent,
    BookCommentComponent,
    LendingDatePickerComponent,
    BookUserProfileCardComponent,
    PasswordChangeConfirmationComponent,
 ],
  imports: [
    SharedModule,
    UserRoutingModule
  ]
})
export class UserModule { }
