import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {BookI} from "../../../model/book.model";
import {BookService} from "../../../service/model-service/book.service";
import {RatingService} from "../../../service/model-service/rating.service";
import {AuthService} from "../../../service/authorization/auth.service";
import {FormBuilder} from "@angular/forms";
import {Constants} from "../../../constants";
import {Observable, Subscription} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap, map} from 'rxjs/operators';
import {LibraryService} from "../../../service/model-service/library.service";
import {LibraryI} from "../../../model/library.model";
import {UserService} from "../../../service/model-service/user.service";
import {UserI} from "../../../model/user.model";
import {UtilsService} from "../../../service/utils.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import { collapseAnimation } from '../../../animations';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-view-ratings',
  templateUrl: './view-ratings.component.html',
  styleUrls: ['./view-ratings.component.css'],
  animations: [collapseAnimation]
})
export class ViewRatingsComponent implements OnInit, OnDestroy {
  @ViewChild('tabs', {static: true})
  private tabs: NgbTabset;
  user: UserI;
  books: BookI[];
  bookModel: Observable<BookI[]>;
  private pagesCount: number;
  private pageNumber: number = 1;
  pageSize: number = 10;
  pageSizeOptions: number[];
  searchPhrase: string = "";
  startAge: number = 1;
  endAge: number = 110;
  selectedValue: string = '1';
  isCollapsed: boolean = true;
  library: LibraryI = null;
  libraries: LibraryI[];
  minimumRating: number = null;
  selectedTab: string = 'all';
  loggedIn: boolean;
  loggedInSubscription: Subscription;
  id: string;
  ageData: Record<number, any> = {
    1: {startAge: 1, endAge: 110, name: "All ages"},
    2: {startAge: 1, endAge: 24, name: "Under 25"},
    3: {startAge: 25, endAge: 49, name: "25-49"},
    4: {startAge: 50, endAge: 110, name: "Over 50"}
  };
  ratingData: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  faCaretDown = faCaretDown;
  
  constructor(private fb: FormBuilder, private ratingService: RatingService, private bookService: BookService,
              private libraryService: LibraryService, private userService: UserService, private authService: AuthService, private utilsService: UtilsService,
              private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['id'];
    this.tabs.activeId = this.id;
    this.selectedTab = this.id;
    this.loggedInSubscription = this.authService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn as boolean;
      if(loggedIn){
        this.getCurrentUser();
      }
    })
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    if(!this.loggedIn){
      this.getLibraries();
      this.getBooks();
    }
  }
  
  ngOnDestroy(){
    this.loggedInSubscription.unsubscribe();
  }
  
  trackByBook(index: number, book: BookI): string { return book.isbn; }
  
  configureTabs(tab) {
    this.selectedTab = tab;
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.pageSize = this.pageSizeOptions[0];
    this.router.navigate(['view-ratings/' + tab]);
    this.resetFilters();
  }

  getBooks() {
    if (this.selectedTab == 'all') {
      this.getRatings();
    }
    else if (this.selectedTab == 'top') {
      this.getTopRatings();
    }
    else if (this.selectedTab == 'recommended') {
      this.getRecommendedBooks();
    }
  }

  resultFormatter(book: BookI) {
    return book.bookTitle + ', ' + book.bookAuthor + ',  ' + book.publisher + ', ' + book.publicationYear + ', ISBN: ' + book.isbn;
  }
  
  inputFormatter(book: BookI) {
    return book.bookTitle;
  }
  
  getLibraries() {
    this.libraryService.getAllLibraries().subscribe(result =>
      this.libraries = result as LibraryI[]);
  }

  changeSearchPhrase(event) {
    this.searchPhrase = event.item ? event.item.isbn : event.target.value;
  }

  search = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.ratingService.getRatings({
        pageNumber: 1,
        pageSize: 10,
        searchPhrase: searchText,
        startAge: this.startAge,
        endAge: this.endAge,
        minimumRating: this.minimumRating,
        libraryId: (this.library ? this.library.libraryId : null)
      })),
      map(x => x['books']));
  };

  changeAgeFilter() {
    this.startAge = this.ageData[this.selectedValue].startAge;
    this.endAge = this.ageData[this.selectedValue].endAge;
  }

  resetFilters() {
    this.searchPhrase = "";
    this.minimumRating = null;
    this.selectedValue = '1';
    this.startAge = this.ageData[this.selectedValue].startAge;
    this.endAge = this.ageData[this.selectedValue].endAge;
    this.library = null;
    this.bookModel = null;
    this.getBooks();
  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(result => {
        this.user = result as UserI;
      }, error => {
      },
      () => {
        if (this.user != null && this.user.preferredLibrary != null) {
          this.library = this.user.preferredLibrary;
        }
        this.getLibraries();
        this.getBooks();
      }
    );
  }
  
  getLibraryService(){
    return this.libraryService;
  }
  
  getRatings() {
    this.ratingService.getRatings({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      searchPhrase: this.searchPhrase,
      startAge: this.startAge,
      endAge: this.endAge,
      minimumRating: this.minimumRating,
      libraryId: (this.library ? this.library.libraryId : null)
    })
      .subscribe(result => {
        this.pagesCount = result["bookCount"] as number;
        this.books = result["books"] as BookI[];
      });
  }

  getTopRatings() {
    this.ratingService.getTopRatings({
      pageSize: this.pageSize,
      startAge: this.startAge,
      endAge: this.endAge,
      libraryId: (this.library ? this.library.libraryId : null)
    })
      .subscribe(result => this.books = result as BookI[]);
  }

  getRecommendedBooks() {
    this.bookService.getRecommendedBooks({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      startAge: this.startAge,
      endAge: this.endAge,
      libraryId: (this.library ? this.library.libraryId : null)
    })
      .subscribe(result => {
        this.pagesCount = result["bookCount"] as number;
        this.books = result["books"] as BookI[];
      });
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageSize = pageSizeOption;
    this.getBooks();
  }

  changePage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.getBooks();
  }
}
