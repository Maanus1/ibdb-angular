import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import {NgbDate, NgbCalendar, NgbDateStruct, NgbDatepickerConfig, NgbDatepicker} from '@ng-bootstrap/ng-bootstrap';
import {NgbDatePipe} from "../../../../../shared/pipe/ngb-date/ngb-date.pipe";
import {UtilsService} from "../../../../../service/utils.service";
import {LentBook} from "../../../../../model/lent-book.model";

@Component({
  selector: 'app-lending-date-picker',
  templateUrl: './lending-date-picker.component.html',
  styleUrls: ['./lending-date-picker.component.css']
})
export class LendingDatePickerComponent implements OnInit {
  @ViewChild('dp', {static: false}) datepicker: NgbDatepicker;
  hoveredDate: NgbDate;
  fromDate: NgbDate;
  toDate: NgbDate;
  @Input() reservedDates: NgbDateStruct[] = [];
  markDisabled;
  @Input() maxLendingPeriod: number;
  @Output() addLentBookEvent: EventEmitter<LentBook> = new EventEmitter();
  
  constructor(private ngbDatePipe: NgbDatePipe, private calendar: NgbCalendar, private config: NgbDatepickerConfig) {
  config.minDate = calendar.getToday();
  let maxDate = calendar.getNext(calendar.getToday(), 'm', 6);
  maxDate.day = 1;
  config.maxDate = maxDate }

  ngOnInit() {
  }
  
  ngOnChanges(changes){
    if(changes.reservedDates.currentValue){
      this.updateDisabledDates();
    }
  }
  
   updateDisabledDates() {
    this.markDisabled = (date: NgbDateStruct) => {
    return !!this.reservedDates.find(x => NgbDate.from(x).equals(date));
   };   
  }
  
    onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      let firstDate = this.getFirstDateNotDisabled(this.reservedDates,this.fromDate,date);
      if (firstDate != null){
      	this.toDate = firstDate;
      }
      else if (date.after(this.calendar.getNext(this.fromDate, 'd', this.maxLendingPeriod - 1))) {
        this.toDate = this.calendar.getNext(this.fromDate, 'd', this.maxLendingPeriod - 1);
      }
      else {
        this.toDate = date;
      }     
    }
    else if (this.fromDate && !this.toDate && date == this.fromDate) {
      this.toDate = date;
    }
    else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  
  getFirstDateNotDisabled(dateList, startDate, endDate){
  	let tempDate = UtilsService.getFirstMatchingDateInList(dateList, startDate, endDate);
      if (tempDate != null) {
        while (UtilsService.getIndexOfNgbDate(tempDate, dateList) != -1) {
          tempDate = this.calendar.getPrev(NgbDate.from(tempDate), 'd');
          tempDate = NgbDate.from({year: tempDate.year, month: tempDate.month, day: tempDate.day});
        }
        return tempDate;
      }
      return null
  }
  
  addLentBook() {
    const lentBook = new LentBook(this.ngbDatePipe.transform(this.fromDate),this.ngbDatePipe.transform(this.toDate));
    this.fromDate = null;
    this.toDate = null;
    this.addLentBookEvent.emit(lentBook);
  }
  
 isOutOfBounds(date: NgbDate) {
 	return date.after(this.getFirstDateNotDisabled(this.reservedDates, this.fromDate, date)) || date.after(this.calendar.getNext(this.fromDate, 'd', this.maxLendingPeriod - 1))
 } 
  
 isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }
} 
