import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef } from '@angular/core';
import {BookCommentService} from "../../../../../service/model-service/book-comment.service";
import {CommentRatingService} from "../../../../../service/model-service/comment-rating.service";
import {BookI} from "../../../../../model/book.model";
import {BookComment, BookCommentI} from "../../../../../model/book-comment.model";
import {CommentRatingI} from "../../../../../model/comment-rating.model";
import { UserMessages } from '../../../../../user-messages';
import {AlertService} from "../../../../../service/alert.service";
import {AuthService} from "../../../../../service/authorization/auth.service";
import {interval, Subscription} from 'rxjs';

@Component({
  selector: 'app-book-comments',
  templateUrl: './book-comments.component.html',
  styleUrls: ['./book-comments.component.css'],
   host: {
    '[class.modal-content]': 'true'
  }
})
export class BookCommentsComponent implements OnInit, OnDestroy {
  book: BookI;
  comment: string;
  bookComments: BookCommentI[];
  key: string = "childComments";
  @ViewChild("modalbody", {static: true})
  modalbody: ElementRef;
  parentCommentsLoaded: number;
  parentCommentsAdded: number;
  currentUser: string;
  isAdmin: boolean;
  bookCommentRefresher: Subscription;
  constructor(private bookCommentService: BookCommentService, private commentRatingService: CommentRatingService, private alertService: AlertService, private authService: AuthService) { }

  ngOnInit() {
  	this.getBookComments();
  	this.parentCommentsLoaded = 10;
  	this.parentCommentsAdded = 10;
  	this.currentUser = this.authService.getCurrentUser();
    this.isAdmin = this.authService.hasAnyRole(['ROLE_ADMIN','ROLE_EMPLOYEE']);
    this.bookCommentRefresher = interval(30000).subscribe(() => {        
        this.getBookComments();
    });
  }
  
  ngOnDestroy(){
    this.bookCommentRefresher.unsubscribe();
  }
  
  onScroll(){
    const el: HTMLElement = this.modalbody.nativeElement;
    if (((el.scrollTop + el.offsetHeight) >= el.scrollHeight * 0.8 )
      && this.parentCommentsLoaded < this.bookComments.length) {
        this.parentCommentsLoaded += this.parentCommentsAdded;
      }
  }

  addComment(comment: BookComment | BookCommentI) {
    if (!comment){
      comment = new BookComment(this.comment, this.book, null);
    }
  	this.bookCommentService.addBookComment(comment).subscribe(() =>{
  	this.alertService.showSuccessAlert(UserMessages.ADD_BOOK_COMMENT_MESSAGE);
  	this.getBookComments();
  	this.comment= null;
  	});
  }
  
  onDeleteCommentRating(id: number){
    this.commentRatingService.deleteCommentRating(id).subscribe(() => {
      this.getBookComments();
    });
  }
  
  updateCommentRating(commentRating: CommentRatingI){
    this.commentRatingService.updateCommentRating(commentRating).subscribe(() => {
      this.getBookComments();
    });
  }
  
  addCommentRating(commentRating: CommentRatingI){
    this.commentRatingService.addCommentRating(commentRating).subscribe(() => {
      this.getBookComments();
    });
  }
  
  onUpdateCommentRating(commentRating: CommentRatingI){
    if(!commentRating.id){
      this.addCommentRating(commentRating);
    }
    else{
      this.updateCommentRating(commentRating);
    }
  }
  
  updateCommentWithRole(comment: BookCommentI){
    if (this.isAdmin){
      return this.bookCommentService.updateBookCommentAsAdmin(comment);
    }
    else{
      return this.bookCommentService.updateBookCommentAsUser(comment);
    }
  }
  
  showUpdateCommentAlert(comment: BookCommentI){
    if(comment.deleteEvent){
  	  this.alertService.showSuccessAlert(UserMessages.DELETE_BOOK_COMMENT_MESSAGE);
  	}
  	else if(comment.editEvent){
  	  this.alertService.showSuccessAlert(UserMessages.EDIT_BOOK_COMMENT_MESSAGE);
  	}
  }
  
  updateComment(comment: BookCommentI) {
  	this.updateCommentWithRole(comment).subscribe(() => {
  	this.showUpdateCommentAlert(comment);
  	this.getBookComments();
  	});
  }
  
  getBookComments() {
    this.bookCommentService.getBookComments(this.book.isbn).subscribe(result => 
    this.bookComments = result as BookCommentI[]);
  }
}
