import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {BookCommentI} from "../../../../../../model/book-comment.model";
import { collapseAnimation } from '../../../../../../animations';
import {BookCommentService} from "../../../../../../service/model-service/book-comment.service";
import {BookI} from "../../../../../../model/book.model";
import {CommentRatingI} from "../../../../../../model/comment-rating.model";
import { UserMessages } from '../../../../../../user-messages';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  animations: [collapseAnimation]
})
export class TreeViewComponent {
  @Input('data') bookComments: BookCommentI[];
  @Input('key') key: string;
  @Input() book: BookI;
  @Input() isRootComment: boolean;
  @Input() commentsLoaded: number;
  @Input() currentUser: number;
  @Input() isAdmin: boolean;
  childCommentsLoaded: number = 3;
  childCommentsAdded: number = 5;
  @Output() updateCommentEvent: EventEmitter<BookCommentI> = new EventEmitter();
  @Output() addCommentEvent: EventEmitter<BookCommentI> = new EventEmitter();
  @Output() deleteCommentRatingEvent: EventEmitter<number> = new EventEmitter();
  @Output() updateCommentRatingEvent: EventEmitter<CommentRatingI> = new EventEmitter();
  constructor(private bookCommentService: BookCommentService) {}
  
  ngOnInit(){
  if(!this.isRootComment){
    this.commentsLoaded = this.childCommentsLoaded;
  }
  }
  
  trackByComment(index: number, comment: BookCommentI): number { return comment.commentId; }
  
  forwardDeleteCommentRatingEvent(id: number){
    this.deleteCommentRatingEvent.emit(id);
  }
  
  forwardUpdateCommentRatingEvent(commentRating: CommentRatingI){
    this.updateCommentRatingEvent.emit(commentRating);
  }
  
  addComment(comment: BookCommentI){
    this.addCommentEvent.emit(comment);
  }
  
  updateComment(comment: BookCommentI){
    this.updateCommentEvent.emit(comment);
  }
}
