import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { collapseAnimation } from '../../../../../../../animations';
import {BookCommentI, BookComment} from "../../../../../../../model/book-comment.model";
import {BookI} from "../../../../../../../model/book.model";
import {CommentRating, CommentRatingI} from "../../../../../../../model/comment-rating.model";
import {faChevronUp, faChevronDown} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-book-comment',
  templateUrl: './book-comment.component.html',
  animations: [collapseAnimation]
})
export class BookCommentComponent implements OnInit, OnChanges {
  @Input() bookComment: BookCommentI;
  @Input('key') key: string;
  @Input() currentUser: number;
  @Input() isAdmin: boolean;
  @Input() book: BookI;
  @Input() isRootComment: boolean;
  @Output() updateCommentEvent: EventEmitter<BookCommentI> = new EventEmitter();
  @Output() updateCommentRatingEvent: EventEmitter<CommentRatingI> = new EventEmitter();
  @Output() deleteCommentRatingEvent: EventEmitter<number> = new EventEmitter();
  @Output() addCommentEvent: EventEmitter<BookComment> = new EventEmitter();
  collapsed: boolean = false;
  editFieldOpen: boolean = false;
  replyFieldOpen: boolean = false;
  newComment: string;
  faChevronUp = faChevronUp;
  faChevronDown = faChevronDown;
  userRating: CommentRatingI;
  ratingTotal: number;
  
  constructor() { }

  ngOnInit() {
  }
  
  ngOnChanges(changes){
      this.userRating = this.bookComment.commentRatings.find(x => x.user == this.currentUser);
      this.ratingTotal = this.bookComment.commentRatings.reduce((accumulator, {rating}) => 
        accumulator + rating, 0
      );
  }
  
  onAddCommentRating(rating:number){
    if(this.userRating){
      this.updateCommentRating(rating)
    }
    else{
      this.addCommentRating(rating);
    }
  }
  
  addCommentRating(rating: number){
    const tempComment = this.cloneComment(this.bookComment);
    const commentRating: CommentRating = new CommentRating(tempComment, rating);
    this.updateCommentRatingEvent.emit(commentRating);
    
  }
  
  updateCommentRating(rating: number){
    const commentRating = this.userRating;
    commentRating.rating = rating;
    commentRating.user = null;
    this.updateCommentRatingEvent.emit(commentRating);
  }
  
  forwardUpdateCommentRatingEvent(commentRating: CommentRatingI){
    this.updateCommentRatingEvent.emit(commentRating);
  }
  
  forwardDeleteCommentRatingEvent(id: number){
    this.deleteCommentRatingEvent.emit(id);
  }
  
  deleteCommentRating(id: number){
    this.deleteCommentRatingEvent.emit(id);
  }
  
  updateComment(comment){
    this.updateCommentEvent.emit(comment);
  }
  
  cloneComment(bookComment: BookComment){
    return {...this.bookComment,
               commentRatings: null,
               childComments: null};
  }
  
  deleteComment(){
    this.bookComment.deleted = true;
    this.bookComment.comment = 'Deleted';
    this.bookComment.deleteEvent = true;
    const tempComment = this.cloneComment(this.bookComment);
    this.updateCommentEvent.emit(tempComment);
  }
  
  editComment(){
    this.bookComment.comment = this.newComment;
    this.bookComment.edited = true;
    this.bookComment.editEvent = true;
    const tempComment = this.cloneComment(this.bookComment);
  	this.updateCommentEvent.emit(tempComment);
  	this.newComment=null;
  	
  	this.editFieldOpen=!this.editFieldOpen
  }
  
  closeTextField(){
    this.newComment = null;
    this.editFieldOpen = false;
    this.replyFieldOpen = false;
  }
  
  addComment(comment: BookComment){
  	this.addCommentEvent.emit(comment);
  }
  
  addChildComment(){
    const tempComment = this.cloneComment(this.bookComment);
    const bookComment = new BookComment(this.newComment, this.book, tempComment);
  	this.addCommentEvent.emit(bookComment);
  	this.newComment=null;
  	this.editFieldOpen=false;
  	this.replyFieldOpen=false;
  }
}
