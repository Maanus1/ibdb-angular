import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import {BookI} from "../../../../model/book.model";
import {BookWaitListItem, BookWaitListItemI} from "../../../../model/book-wait-list-item.model";
import {BookCopyI} from "../../../../model/book-copy.model";
import {LentBookI} from "../../../../model/lent-book.model";
import {RatingService} from "../../../../service/model-service/rating.service";
import {BookCopyService} from "../../../../service/model-service/book-copy.service";
import {LentBookService} from "../../../../service/model-service/lent-book.service";
import {UtilsService} from "../../../../service/utils.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Rating, RatingI} from "../../../../model/rating.model";
import {NgbDate, NgbCalendar, NgbDateStruct, NgbDatepickerConfig, NgbDatepicker} from '@ng-bootstrap/ng-bootstrap';
import {LibraryService} from "../../../../service/model-service/library.service";
import {LibraryI} from "../../../../model/library.model";
import {BookWaitListItemService} from "../../../../service/model-service/book-wait-list-item.service";
import {AuthService} from "../../../../service/authorization/auth.service";
import {AlertService} from "../../../../service/alert.service";
import {tap, map, mergeMap, concatMap} from 'rxjs/operators';
import {Observable, forkJoin, Subscription} from 'rxjs';
import {UserMessages} from "../../../../user-messages";
import {BookCommentsComponent} from "./book-comments/book-comments.component";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {NgbDatePipe} from "../../../../shared/pipe/ngb-date/ngb-date.pipe";

@Component({
  selector: 'app-view-book-rating',
  templateUrl: './view-book-rating.component.html',
  styles: []
})
export class ViewBookRatingComponent implements OnInit, OnDestroy {
  @ViewChild('dp', {static: false}) datepicker: NgbDatepicker;
  
  id: string;
  @Input() book: BookI;
  rating: Rating | RatingI;
  loggedIn:boolean = false;
  startAge: number = 1;
  endAge: number = 110;
  totalRating: Observable<number>;
  ratingUnder25: Observable<number>;
  rating25To50: Observable<number>;
  ratingOver50: Observable<number>;
  bookCopy: BookCopyI;
  bookCopies: BookCopyI[] = [];
  lentBooks: LentBookI[];
  bookReservedDates: NgbDateStruct[] = [];
  lentBook: LentBookI;
  library: LibraryI = null;
  libraries: LibraryI[];
  bookWaitListItem: BookWaitListItemI;
  maxLendingPeriod: number = 28;
  loggedInSubscription: Subscription;
  constructor(private ratingService: RatingService, private router: Router,
              private activatedRoute: ActivatedRoute, private calendar: NgbCalendar,
              private bookCopyService: BookCopyService, private lentBookService: LentBookService,
              private utilsService: UtilsService, private config: NgbDatepickerConfig,
              private libraryService: LibraryService, private bookWaitListItemService: BookWaitListItemService,
              private authService: AuthService, private alertService: AlertService, private modalService: NgbModal, private ngbDatePipe: NgbDatePipe) {
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.params['isbn'];
    let bookObservable: Observable<BookI> = this.ratingService.getBookWithRating(this.id);
    let librariesObservable: Observable<LibraryI[]> =  this.libraryService.getAllLibraries();
    let bookCopyObservable: Observable<BookCopyI[]> = this.bookCopyService.getBookCopiesByUser(this.id, null);
    this.loggedInSubscription = this.authService.loggedIn.subscribe(loggedIn => {
	    this.loggedIn = loggedIn as boolean
	    if (this.loggedIn){
	      this.ratingService.getCurrentUserRating(this.id).subscribe(result => {
		    this.rating = result as RatingI;
		      if (!this.rating) {
		        this.rating = new Rating(0);
		      }
		      });
		   }
    });
    forkJoin(bookObservable, librariesObservable, bookCopyObservable)
    .subscribe(([book,libraries,bookCopies]) => {
      this.book = book as BookI;
      this.libraries = libraries as LibraryI[];      
      
      this.bookCopies = bookCopies as BookCopyI[];
      if (this.bookCopies.length > 0) {
        this.bookCopy = this.bookCopies[0];
        this.getBookReservedDays();
      }
      this.getWaitListItem();
      this.getRatings();    
    });
    
  }
  
  ngOnDestroy(){
    this.loggedInSubscription.unsubscribe();
  }
  
  getRatings(){
    this.totalRating = this.getRating(1, 110);
    this.ratingUnder25 = this.getRating(1, 25);
    this.rating25To50 = this.getRating(26, 50);
    this.ratingOver50 = this.getRating(51, 110);
  }
  
  getLibraryService(){
    return this.libraryService;
  }
  
  openComments() {
    const modalRef = this.modalService.open(BookCommentsComponent, {windowClass:'custom-modal', scrollable: true,size:'xl'});
    modalRef.componentInstance.book = this.book;
  }

  getBookReservedDays() {
    if (this.loggedIn == false || this.book.available == false) {
      return;
    }
    this.lentBookService.getBookReservedDays(this.bookCopy.bookId).subscribe(result => {
      let bookedDates = result as Date[];
      this.bookReservedDates = UtilsService.convertDatesToNgbDateStructs(bookedDates);
    });
  }

  getBookCopies() {
    this.bookCopyService.getBookCopiesByUser(this.id, {libraryId: (this.library ? this.library.libraryId : null)}).subscribe(result => {
      this.bookCopies = result as BookCopyI[];
      if (this.bookCopies.length > 0) {
        this.bookCopy = this.bookCopies[0];
        this.getBookReservedDays();
      }
      else if(this.loggedIn){
        this.getWaitListItem();
      }
    });
  }

  addLentBook(lentBook) {
    lentBook.bookCopy = this.bookCopy;
    lentBook.library = this.bookCopy.library;
    this.lentBookService.addLentBook(lentBook).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.ADD_BOOK_RESERVATION_MESSAGE);
      this.getBookReservedDays();
    });
  }
  
  getRating(startAge: number, endAge: number): Observable<number> {
    return this.ratingService.getRating(this.id, {startAge: startAge, endAge: endAge});
  }
  
  changeRating(){
    if (this.rating.ratingId){
      this.updateRating();
    }
    else{
      this.addRating();
    }
  }
  
  updateRating() {
    this.ratingService.updateRating(this.rating)
      .subscribe(() => this.getRatings());
  }
  
  addRating() {
    this.rating.book = this.book;
    this.ratingService.addRating(this.rating)
      .subscribe(result => {
      this.rating = result as RatingI;
      this.getRatings();})
  }

  addToWaitList() {
    this.bookWaitListItemService.addToWaitList(new BookWaitListItem(this.library,this.book)).subscribe(() => this.getWaitListItem())
  }

  deleteFromWaitList() {
    this.bookWaitListItemService.deleteFromWaitList(this.bookWaitListItem.waitListId).subscribe(() => this.getWaitListItem())
  }

  getWaitListItem() {
    if (!this.loggedIn || this.bookCopies.length > 0){
      return;
    }
    this.bookWaitListItemService.getWaitListItem(this.book.isbn,
    {
      libraryId: (this.library ? this.library.libraryId : null),
    }).subscribe(result => this.bookWaitListItem = result as BookWaitListItemI)
  }
}
