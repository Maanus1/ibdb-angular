import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, ValidationErrors, AbstractControl} from "@angular/forms";
import {Router} from "@angular/router";
import {UserI} from "../../../model/user.model";
import {LibraryI} from "../../../model/library.model";
import {UserService} from "../../../service/model-service/user.service";
import {LibraryService} from "../../../service/model-service/library.service";
import {AuthService} from "../../../service/authorization/auth.service";
import {HttpParams} from "@angular/common/http";
import {UtilsService} from "../../../service/utils.service";
import { validFormAnimation } from '../../../animations';
import {Observable} from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import {ValidatorService} from "../../../service/validator.service";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  animations: [validFormAnimation]
})

export class CreateAccountComponent implements OnInit {

  id: number;
  userForm: FormGroup;
  libraries: LibraryI[] = [];
  constructor(private fb: FormBuilder, private router: Router, private userService: UserService, private authService: AuthService, private utilsService: UtilsService, private validatorService: ValidatorService, private libraryService: LibraryService) {
  }


  ngOnInit() {
    this.libraryService.getAllLibraries()
             .subscribe(libraries=>
             this.libraries = libraries as LibraryI[]);
  }

  
  
  saveUser(user: UserI) {
    this.userService.createUser(user)
      .subscribe(() => this.login(user));
  }
  
  login(user: UserI) {
    this.authService.login(user.email, user.password).subscribe(() =>this.router.navigate(['/']));
  }
}
