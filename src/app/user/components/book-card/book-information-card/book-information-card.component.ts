import {Component, OnInit, Input} from '@angular/core';
import {BookI} from "../../../../model/book.model";
import {Constants} from "../../../../constants";

@Component({
  selector: 'app-book-information-card',
  templateUrl: './book-information-card.component.html'
})
export class BookInformationCardComponent implements OnInit {
  @Input() book: BookI;
  bookAvailabilityMap: Record<string, string>;	
	
  constructor() {
  }

  ngOnInit() {
  this.bookAvailabilityMap = Constants.AVAILABILITY
  }

}
