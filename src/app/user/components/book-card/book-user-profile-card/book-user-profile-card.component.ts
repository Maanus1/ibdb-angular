import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {LentBookI} from "../../../../model/lent-book.model";
import {BookI} from "../../../../model/book.model";

@Component({
  selector: 'app-book-user-profile-card',
  templateUrl: './book-user-profile-card.component.html'
})
export class BookUserProfileCardComponent implements OnInit {
  @Input() book: BookI;
  @Input() lentBook: LentBookI;
  @Output() deleteEvent: EventEmitter<void> = new EventEmitter();
    
  constructor() { }

  ngOnInit() {
  }
  
  deleteItem(){
      this.deleteEvent.emit();
  } 
}
