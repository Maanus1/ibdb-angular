import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {NotificationService} from "../../../../service/model-service/notification.service";
import {NotificationI} from "../../../../model/notification.model";
import {Constants} from "../../../../constants";
import {UtilsService} from "../../../../service/utils.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html'
})
export class UserNotificationsComponent implements OnInit {
  private pagesCount: number;
  private pageNumber: number = 1;
  pageSize: number = 10;
  pageSizeOptions: number[];
  notifications: NotificationI[];

  constructor(private notificationService: NotificationService, private router: Router) {
  }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getNotifications();
    this.getNotificationCount();
  }
  
  trackByNotification(index: number, notification: NotificationI): number { return notification.notificationId; }
  
  getNotifications() {
    this.notificationService.getNotifications({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    }).subscribe(result => this.notifications = result as NotificationI[]);
  }
  
  navigateToRoute(notification){
  	let notificationType = notification.notificationType;
  	if (notificationType == 'USER_INFORMATION'){
  		this.router.navigate(['/user-profile/account']);
  	}
  	else if (notificationType == 'LENDING_ACTIVATED' || notificationType == 'RESERVATION_CANCELLED' || notificationType == 'BOOK_RETURNED'){
  		this.router.navigate(['/user-profile/lendings']);
  	}
  	else if (notificationType == 'BOOK_AVAILABLE' || notificationType == 'COMMENT_REPLY'){
  		this.router.navigate(['/view-ratings/book',  notification.book.isbn]);
  	}
  }
  
  getNotificationCount() {
    this.notificationService.getNotificationCount().subscribe(result => this.pagesCount = result as number);
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageSize = pageSizeOption;
    this.getNotifications();
  }

  changePage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.getNotifications();
  }
}
