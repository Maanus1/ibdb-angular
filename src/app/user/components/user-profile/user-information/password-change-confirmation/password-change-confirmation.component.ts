import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-password-change-confirmation',
  templateUrl: './password-change-confirmation.component.html'
})
export class PasswordChangeConfirmationComponent implements OnInit {
  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  changePassword() {
    this.activeModal.close(true);
  }
}
