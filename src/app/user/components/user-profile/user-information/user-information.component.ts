import {Component, OnInit, Input, Output, EventEmitter, ViewChild} from '@angular/core';
import {UtilsService} from "../../../../service/utils.service";
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {PasswordChangeConfirmationComponent} from './password-change-confirmation/password-change-confirmation.component';
import {UserInformationChangeConfirmationComponent} from './user-information-change-confirmation/user-information-change-confirmation.component';
import {UserI} from "../../../../model/user.model";
import {UserService} from "../../../../service/model-service/user.service";
import {LibraryI} from "../../../../model/library.model";
import {LibraryService} from "../../../../service/model-service/library.service";
import {AlertService} from "../../../../service/alert.service";
import {PasswordFormComponent} from "../../../../shared/components/forms/password-form/password-form.component";
import { collapseAnimation } from '../../../../animations';
import { UserMessages} from '../../../../user-messages';
import {forkJoin, Observable} from 'rxjs';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.component.html',
  animations: [collapseAnimation]
})
export class UserInformationComponent implements OnInit {
  @ViewChild(PasswordFormComponent, {static: false}) 
  private passwordForm: PasswordFormComponent;
  user: UserI;
  library: LibraryI;
  libraries: LibraryI[];

  constructor(private utilsService: UtilsService, private modalService: NgbModal, private userService: UserService, private libraryService: LibraryService, private alertService: AlertService) {
  }

  ngOnInit() {
    let librariesObservable: Observable<LibraryI[]> = this.libraryService.getAllLibraries();
    let userObservable: Observable<UserI> = this.userService.getCurrentUser();
    forkJoin(librariesObservable, userObservable)
             .subscribe(([libraries,user])=>{
             this.libraries = libraries as LibraryI[];
             this.user = user as UserI;
             });
  }
  
  saveUser(user: UserI){
     this.userService.updateUser(user).subscribe(() => {
       this.alertService.showSuccessAlert(UserMessages.UPDATE_USER_INFORMATION_MESSAGE);
     });
  }
  
  onSaveUser(user: UserI) {
    const modalRef: NgbModalRef = this.modalService.open(UserInformationChangeConfirmationComponent);
    modalRef.result.then((result) => {
      if (result) {
        this.saveUser(user);
      }
    });
  }
  
  onChangePassword(passwords){
    this.userService.changePassword({currentPassword: passwords.currentPassword,
                                     newPassword: passwords.newPassword}).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.CHANGE_PASSWORD_MESSAGE);
      this.passwordForm.resetForm();
    });
  }
  
  changePassword(passwords) {
    const modalRef: NgbModalRef = this.modalService.open(PasswordChangeConfirmationComponent);
    modalRef.result.then((result) => {
      if (result) {
        this.changePassword(passwords);
      }
    });
  }
}
