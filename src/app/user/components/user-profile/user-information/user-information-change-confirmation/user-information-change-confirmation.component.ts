import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserI} from "../../../../../model/user.model";

@Component({
  selector: 'app-user-information-change-confirmation',
  templateUrl: './user-information-change-confirmation.component.html'
})
export class UserInformationChangeConfirmationComponent implements OnInit {
  constructor(private activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  close() {
    this.activeModal.close(false);
  }

  updateUser() {
    this.activeModal.close(true);
  }

}
