import {Component, OnInit, ViewChild, ChangeDetectorRef, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router, NavigationEnd, RouterEvent} from "@angular/router";
import {NgbTabset} from '@ng-bootstrap/ng-bootstrap';
import {filter} from 'rxjs/operators';
import {pipe, Subscription} from 'rxjs';
import { trigger, state, style, animate, transition} from '@angular/animations';
import {fadeAnimation} from "../../../animations";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements AfterViewInit {
  @ViewChild('tabs', {static: true})
  private tabs: NgbTabset;
  
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private cdRef : ChangeDetectorRef) {
  }
  
  ngAfterViewInit() {
     const urlSegments = this.router.routerState.snapshot.url.split('/');
     this.tabs.select(urlSegments[urlSegments.length-1]);
     this.cdRef.detectChanges();
  }
  
  configureTabs(tab) {
    this.router.navigate(['user-profile/' + tab]);
  }

}
