import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {RatingService} from "../../../../service/model-service/rating.service";
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {BookI} from "../../../../model/book.model";
import {RatingI} from "../../../../model/rating.model";
import {Constants} from "../../../../constants";
import {UtilsService} from "../../../../service/utils.service";
import {AlertService} from "../../../../service/alert.service";
import {UserMessages} from "../../../../user-messages";
import { collapseAnimation2 } from '../../../../animations';
import {faStar} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-user-ratings',
  templateUrl: './user-ratings.component.html',
  styleUrls: ['./user-ratings.component.css'],
  animations: [ collapseAnimation2 ]
})
export class UserRatingsComponent implements OnInit {
  searchPhrase: string = "";
  private pagesCount: number;
  private pageNumber: number = 1;
  pageSize: number = 10;
  pageSizeOptions: number[];
  bookModel: Observable<RatingI>;
  books: RatingI[];
  faStar = faStar
  
  constructor(private ratingService: RatingService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getData();
  }
  
  trackByBook(index: number, book: BookI): string { return book.isbn; }
  
  getData() {
    this.getCurrentUserRatings();
    this.getCurrentUserRatingCount();
  }

  updateRating(rating: RatingI) {
    this.ratingService.updateRating(rating).subscribe();
  }

  searchUserRatings = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.ratingService.getCurrentUserRatings({
        pageNumber: 1,
        pageSize: 10,
        searchPhrase: searchText
      })));
  };

  filterBooks(event) {
    this.searchPhrase = event.item ? event.item.book.isbn : event.target.value;
    this.getCurrentUserRatings();
    this.getCurrentUserRatingCount()
  }

  resultFormatter(book: RatingI) {
    return book.book.bookTitle + ', ' + book.book.bookAuthor + ',  ' + book.book.publisher + ', ' + book.book.publicationYear + ', ISBN: ' + book.book.isbn;
  }
  
  inputFormatter(book: RatingI) {
    return book.book.bookTitle;
  }
  
  deleteRating(ratingId: number) {
    this.ratingService.deleteRating(ratingId)
      .subscribe(() => {
        this.alertService.showSuccessAlert(UserMessages.DELETE_RATING_MESSAGE);
        this.getCurrentUserRatingCount();
        this.getCurrentUserRatings();
      });
  }


  changePageSizeOption(pageSizeOption: number) {
    this.pageSize = pageSizeOption;
    this.getCurrentUserRatings();
  }

  changePage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.getCurrentUserRatings();
  }

  getCurrentUserRatingCount() {
    this.ratingService.getUserRatingsCount({searchPhrase: this.searchPhrase}).subscribe(result =>
      this.pagesCount = result as number);
  }

  getCurrentUserRatings() {
    this.ratingService.getCurrentUserRatings({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      searchPhrase: this.searchPhrase
    })
      .subscribe(result => this.books = result as RatingI[]);
  }
  
  mouseLeave(book){
      setTimeout(() => book.open = book.hovering ? true : false, 500);
  }
  
  mouseEnter(book){
    book.open = true;
  }
  
  mouseHovering(book){
    book.hovering = true;
  }
  
  mouseOut(book){
    book.hovering = false;
  }
  }
