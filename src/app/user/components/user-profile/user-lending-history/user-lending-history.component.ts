import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {LentBookI} from "../../../../model/lent-book.model";
import {LibraryI} from "../../../../model/library.model";
import {BookI} from "../../../../model/book.model";
import {LentBookService} from "../../../../service/model-service/lent-book.service";
import {UtilsService} from "../../../../service/utils.service";
import {LibraryService} from "../../../../service/model-service/library.service";
import {BookReservationCancellationConfirmationComponent} from '../../../../shared/components/modal/book-reservation-cancellation-confirmation/book-reservation-cancellation-confirmation.component';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {Constants} from "../../../../constants";
import {AlertService} from "../../../../service/alert.service";
import {NgbDatePipe} from "../../../../shared/pipe/ngb-date/ngb-date.pipe";
import { collapseAnimation } from '../../../../animations';
import { UserMessages } from '../../../../user-messages';
import {faCaretDown} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-lending-history',
  templateUrl: './user-lending-history.component.html',
  animations: [collapseAnimation]
})
export class UserLendingHistoryComponent implements OnInit {
  lendingStatuses: string[];
  lendingStatus: string = null;
  private pagesCount: number;
  private pageNumber: number = 1;
  pageSize: number = 10;
  pageSizeOptions: number[];
  searchPhrase: string = "";
  @Input() lentBook: LentBookI;
  fromDate: NgbDateStruct = null;
  toDate: NgbDateStruct = null;
  isCollapsed: boolean = true;
  library: LibraryI = null;
  libraries: LibraryI[];
  lentBooks: LentBookI[];
  bookLendingStatuses: Record<string, string>;
  bookModel: Observable<BookI[]>;
  faCaretDown = faCaretDown;

  constructor(private lentBookService: LentBookService, private utilsService: UtilsService, private libraryService: LibraryService, private modalService: NgbModal, private alertService: AlertService, private ngbDatePipe: NgbDatePipe) {
  }

  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.bookLendingStatuses = Constants.BOOK_LENDING_STATUSES;
    this.getLibraries();
    this.getLendingStatuses();
    this.getData();
  }
  
  trackByLentBook(index: number, lentBook: LentBookI): number { return lentBook.id; }
  
  searchLentBooks = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.lentBookService.getCurrentUserUniqueLentBooks({
        pageNumber: 1,
        pageSize: 10,
        searchPhrase: searchText,
        lendingStatus: this.lendingStatus,
        fromDate: this.ngbDatePipe.transform(this.fromDate),
        toDate: this.ngbDatePipe.transform(this.toDate),
        libraryId: (this.library ? this.library.libraryId : null)
      }))
    );
  };

  changeLibraryDropdownValue(item) {
    if (item == 'All libraries') {
      this.library = null;
    }
    else {
      this.library = item;
    }
  }

  getLibraries() {
    this.libraryService.getAllLibraries().subscribe(result =>
      this.libraries = result as LibraryI[])
  }
  
  cancelBookLending(id: number){
    this.lentBookService.cancelBookLending(id).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.CANCEL_BOOK_LENDING_MESSAGE);
      this.getCurrentUserLentBooks();
      this.getLendingStatuses();
    });
  }
  
  onCancelBookLending(lentBook: LentBookI) {
    const modalRef: NgbModalRef = this.modalService.open(BookReservationCancellationConfirmationComponent);
    modalRef.componentInstance.lentBook = lentBook;
    modalRef.result.then((result) => {
      if (result) {
        this.cancelBookLending(lentBook.id);
      }
    });
  }

  getCurrentUserLentBookCount() {
    this.lentBookService.getUserLentBookCount({
      searchPhrase: this.searchPhrase,
      lendingStatus: this.lendingStatus,
      fromDate: this.ngbDatePipe.transform(this.fromDate),
      toDate: this.ngbDatePipe.transform(this.toDate),
      libraryId: (this.library ? this.library.libraryId : null)
    }).subscribe(result =>
      this.pagesCount = result as number)
  }

  getLendingStatuses() {
    this.lentBookService.getLendingStatuses().subscribe(result => this.lendingStatuses = result as string[]);
  }

  getCurrentUserLentBooks() {
    this.lentBookService.getCurrentUserLentBooks({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      searchPhrase: this.searchPhrase,
      lendingStatus: this.lendingStatus,
      fromDate: this.ngbDatePipe.transform(this.fromDate),
      toDate: this.ngbDatePipe.transform(this.toDate),
      libraryId: (this.library ? this.library.libraryId : null)
    })
      .subscribe(result => this.lentBooks = result as LentBookI[]);
  }

  resultFormatter(book: BookI) {
    return book.bookTitle + ', ' + book.bookAuthor + ',  ' + book.publisher + ', ' + book.publicationYear + ', ISBN: ' + book.isbn;
  }
  
  inputFormatter(book: BookI) {
    return book.bookTitle;
  }
  
  changeSearchPhrase(event) {
    this.searchPhrase = event.item ? event.item.isbn : event.target.value;
  }

  resetFilter() {
    this.searchPhrase = "";
    this.bookModel = undefined;
    this.fromDate = null;
    this.toDate = null;
    this.lendingStatus = null;
    this.library = null;
    this.getData();
  }

  getData() {
    this.getCurrentUserLentBooks();
    this.getCurrentUserLentBookCount();
  }

  filterLentBooks() {
    this.getCurrentUserLentBookCount();
    this.getCurrentUserLentBooks();
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageSize = pageSizeOption;
    this.getCurrentUserLentBooks();
  }

  changePage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.getCurrentUserLentBooks();
  }
}
