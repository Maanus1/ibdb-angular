import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {BookWaitListItemService} from "../../../../service/model-service/book-wait-list-item.service";
import {LibraryI} from "../../../../model/library.model";
import {BookI} from "../../../../model/book.model";
import {BookWaitListItemI} from "../../../../model/book-wait-list-item.model";
import {Constants} from "../../../../constants";
import {LibraryService} from "../../../../service/model-service/library.service";
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {UtilsService} from "../../../../service/utils.service";
import {AlertService} from "../../../../service/alert.service";
import {UserMessages} from "../../../../user-messages";

@Component({
  selector: 'app-user-wait-list',
  templateUrl: './user-wait-list.component.html'
})
export class UserWaitListComponent implements OnInit {
  private waitListCount: number;
  private pageNumber: number = 1;
  pageSize: number = 10;
  pageSizeOptions: number[];
  library: LibraryI = null;
  searchPhrase: string = "";
  libraries: LibraryI[];
  waitList: BookWaitListItemI[];
  bookModel: Observable<BookI[]>;

  constructor(private utilsService: UtilsService, private bookWaitListItemService: BookWaitListItemService, private libraryService: LibraryService, private alertService: AlertService) {
  }
 
  ngOnInit() {
    this.pageSizeOptions = Constants.PAGE_SIZE_OPTIONS;
    this.getLibraries();
    this.getData();
  }
  
  trackByWaitListItem(index: number, waitListItem: BookWaitListItemI): number { return waitListItem.waitListId; }
  
  inputFormatter(book: BookI) {
    return book.bookTitle;
  }
  
  resultFormatter(book: BookI) {
    return book.bookTitle + ', ' + book.bookAuthor + ',  ' + book.publisher + ', ' + book.publicationYear + ', ISBN: ' + book.isbn;
  }
  
  
  getData() {
    this.getUserWaitList();
    this.getUserWaitListCount();
  }

  getUserWaitList() {
    this.bookWaitListItemService.getUserWaitList({
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      searchPhrase: this.searchPhrase,
      libraryId: (this.library ? this.library.libraryId : null)
    }).subscribe(result => this.waitList = result as BookWaitListItemI[]);
  }

  getUserWaitListCount() {
    this.bookWaitListItemService.getUserWaitListCount({
      searchPhrase: this.searchPhrase,
      libraryId: (this.library ? this.library.libraryId : null)
    }).subscribe(result => this.waitListCount = result as number);
  }

  deleteFromWaitList(waitListId: number) {
    this.bookWaitListItemService.deleteFromWaitList(waitListId).subscribe(() => {
      this.alertService.showSuccessAlert(UserMessages.DELETE_WAIT_LIST_ITEM_MESSAGE);
      this.getData();
    })
  }
  
  filterWaitListUniqueBooks(event) {
    this.searchPhrase = event.item ? event.item.isbn : event.target.value;
    this.getData();
  }

  getLibraries() {
    this.libraryService.getAllLibraries().subscribe(result =>
      this.libraries = result as LibraryI[]);
  }

  changePageSizeOption(pageSizeOption: number) {
    this.pageSize = pageSizeOption;
    this.getUserWaitList();
  }

  changePage(pageNumber: number) {
    this.pageNumber = pageNumber;
    this.getUserWaitList();
  }

  searchBookWaitList = (text$: Observable<string>) => {
    return text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((searchText) => this.bookWaitListItemService.getUserWaitListUniqueBooks({
        pageNumber: 1,
        pageSize: 10, 
        searchPhrase: searchText, 
        libraryId: (this.library ? this.library.libraryId : null)
      }))
    );
  }
}
