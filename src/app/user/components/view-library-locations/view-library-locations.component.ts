import {Component, OnInit} from '@angular/core';
import {LibraryI} from "../../../model/library.model";
import {LibraryService} from "../../../service/model-service/library.service";

@Component({
  selector: 'app-view-library-locations',
  templateUrl: './view-library-locations.component.html',
  styleUrls: ['./view-library-locations.component.css']
})
export class ViewLibraryLocationsComponent implements OnInit {
  libraries: LibraryI[];

  constructor(private libraryService: LibraryService) {
  }

  ngOnInit() {
    this.getAllLibraries();
  }

  getAllLibraries() {
    this.libraryService.getAllLibraries().subscribe(result => this.libraries = result as LibraryI[]);
  }

}
