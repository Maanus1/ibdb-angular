export class Constants {
  public static readonly PAGE_SIZE_OPTIONS: number[] = [10, 20, 30];

  public static readonly DATE_FORMAT_SEPARATOR: string = "/";
  
  public static readonly LANGUAGE: string = 'LANGUAGE';
 
  public static readonly REGEX_PATTERN_DECIMAL: string = "^[0-9]+\\.?[0-9]*$";
  
  public static readonly REFRESH_TOKEN_ERROR_DESCRIPTION: string = 'Invalid refresh token';

  public static readonly BOOK_LENDING_STATUSES: Record<string, string> = {BOOK_RESERVED: 'Reserved',
																	      CANCELLED: 'Cancelled',
																	      ACTIVE: 'Active',
																	      BOOK_RETURNED: 'Returned'}

  public static readonly AVAILABILITY: Record<string, string> = {false: 'Unavailable',
															     true: 'Available'}
  
  public static readonly LANGUAGES: Record<string, string> = {et_EE: 'EST', 
                                                              en_US: 'ENG' }
}
