export enum RequestType {
  POST,
  DELETE,
  PUT,
  GET
}
